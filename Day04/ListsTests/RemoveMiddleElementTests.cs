﻿using Day04Tasks;
namespace ListsTests;

[TestClass]
public class RemoveMiddleElementTests
{
    //Testing RemoveMiddleElement
    [TestMethod]
    public void TestRemoveMiddleElementOdd()
    {
        //Assing
        LinkedList<int> list = new LinkedList<int>(new int[] { 1, 2, 3, 4, 5 });
        LinkedList<int> expected = new LinkedList<int>(new int[] { 1, 2, 4, 5 });
        //Act
        ListsHomeWork.RemoveMiddleElement(list);

        //Assert
        CollectionAssert.AreEqual(expected, list);
    }

    [TestMethod]
    public void TestRemoveMiddleElementEven()
    {
        //Assing
        LinkedList<int> list = new LinkedList<int>(new int[] { 1, 2, 3, 4, 5, 6 });
        LinkedList<int> expected = new LinkedList<int>(new int[] { 1, 2, 3, 5, 6 });
        //Act
        ListsHomeWork.RemoveMiddleElement(list);

        //Assert
        CollectionAssert.AreEqual(expected, list);
    }

    [TestMethod]
    public void TestRemoveMiddleWhenHasSameValue()
    {
        //Assing
        LinkedList<int> list = new LinkedList<int>(new int[] { 3, 2, 3, 4, 5, 6 });
        LinkedList<int> expected = new LinkedList<int>(new int[] { 3, 2, 3, 5, 6 });
        //Act
        ListsHomeWork.RemoveMiddleElement(list);

        //Assert
        CollectionAssert.AreEqual(expected, list);
    }
}
