﻿namespace Day06_Trees;

public class MergeLists
{
    public static int[] MergeListsFunc(int[,] lists)
    {
        if (lists.Length == 0)
        {
            throw new ArgumentException("Collection is empty!");
        }

        PriorityQueue<int, int> elements = new PriorityQueue<int, int>();
        for (int row = 0; row < lists.GetLength(0); row++)
        {
            for (int col = 0; col < lists.GetLength(1); col++)
            {
                elements.Enqueue(lists[row, col], lists[row, col]);
            }
        }

        int[] result = new int[elements.Count];
        int index = 0;
        while (elements.Count > 0)
        {
            result[index++] = elements.Dequeue();
        }

        return result;
    }
}
