﻿using Day06_Trees;

public class TreeHeight
{
    public static int GetTreeHeight(BinaryTreeNode root)
    {
        if (root == null)
        {
            return 0;
        }

        int lHeight = 0;
        if (root.Left != null)
        {
            lHeight += GetLeafsHeight(root.Left);
        }

        int rHeight = 0;
        if (root.Right != null)
        {
            rHeight += GetLeafsHeight(root.Right);
        }

        int maxHeight = rHeight > lHeight ? rHeight : lHeight;

        return maxHeight;
    }
    // малко ми е зор да си преставя рекурсиите без два метода, защото винаги имам да уникални
    // действия в първия, освен ако не се дефинират извън самия метод по някакъв начин
    public static int GetLeafsHeight(BinaryTreeNode root)
    {
        int lHeight = 0;
        if (root.Left != null)
        {
            lHeight += GetLeafsHeight(root.Left);
        }

        int rHeight = 0;
        if (root.Right != null)
        {
            rHeight += GetLeafsHeight(root.Right);
        }

        int maxHeight = (rHeight > lHeight ? rHeight : lHeight) + 1;

        return maxHeight;
    }
}
