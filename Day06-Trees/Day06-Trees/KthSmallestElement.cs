﻿namespace Day06_Trees;

using System.Reflection.Metadata.Ecma335;

public class KthSmallestElement
{
    public static int GetKthSmallestElement(BinaryTreeNode root, int kSmallestElement)
    {
        if (root == null)
        {
            throw new ArgumentNullException("Tree is empty!");
        }

        List<int> values = new List<int>();

        InOrder(root, values);
        //values.Sort();

        return values[kSmallestElement - 1];
    }
    public static void InOrder(BinaryTreeNode root, List<int> values)
    {
        if (root.Left != null)
        {
            InOrder(root.Left, values);
        }

        values.Add(root.Value);

        if (root.Right != null)
        {
            InOrder(root.Right, values);
        }
    }

}
