﻿namespace Day06_Trees;

public class GetOrders
{
    //StringWriter test and logic are in the last method.
    public static string PreOrder(BinaryTreeNode root)
    {
        if (root == null)
        {
            return "Tree is empty";
        }

        List<int> values = new List<int>();
        values.Add(root.Value);

        if (root.Left != null)
        {
            SubPreOrder(root.Left, values);
        }

        if (root.Right != null)
        {
            SubPreOrder(root.Right, values);
        }

        return string.Join(", ", values);
    }
    private static void SubPreOrder(BinaryTreeNode root, List<int> values)
    {
        values.Add(root.Value);
        if (root.Left != null)
        {
            SubPreOrder(root.Left, values);
        }

        if (root.Right != null)
        {
            SubPreOrder(root.Right, values); ;
        }
    }

    public static string PostOrder(BinaryTreeNode root)
    {
        if (root == null)
        {
            return "Tree is empty";
        }

        List<int> values = new List<int>();
        if (root.Left != null)
        {
            SubPostOrder(root.Left, values);
        }

        if (root.Right != null)
        {
            SubPostOrder(root.Right, values);
        }

        values.Add(root.Value);

        return string.Join(", ", values);
    }

    private static void SubPostOrder(BinaryTreeNode root, List<int> values)
    {
        if (root.Left != null)
        {
            SubPostOrder(root.Left, values);
        }

        if (root.Right != null)
        {
            SubPostOrder(root.Right, values);
        }

        values.Add(root.Value);
    }
    public static string InOrder(BinaryTreeNode root)
    {
        if (root == null)
        {
            return "Tree is empty";
        }

        List<int> values = new List<int>();
        if (root.Left != null)
        {
            SubInOrder(root.Left, values);
        }
        
        values.Add(root.Value);

        if (root.Right != null)
        {
            SubInOrder(root.Right, values);
        }


        return string.Join(", ", values);
    }

    private static void SubInOrder(BinaryTreeNode root, List<int> values)
    {
        if (root.Left != null)
        {
            SubInOrder(root.Left, values);
        }

        values.Add(root.Value);

        if (root.Right != null)
        {
            SubInOrder(root.Right, values);
        }

    }

    public static void SWInOrder(BinaryTreeNode root, StreamWriter writer)
    {

        if (root.Left != null)
        {
            SWInOrder(root.Left, writer);
        }

        writer.Write(root.Value);

        if (root.Right != null)
        {
            SWInOrder(root.Right,writer);
        }
    }

 }
