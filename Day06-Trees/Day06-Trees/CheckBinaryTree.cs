﻿namespace Day06_Trees;

public class CheckBinaryTree
{
    public static bool IsBinaryTree(BinaryTreeNode root, int topBorder, int bottomBorder)
    {
        if (root == null)
        {
            throw new ArgumentException("Tree is empty!");
        }

        if (root.Left == null && root.Right == null)
        {
            return true;
        }

        if (root.Left != null && root.Left.Value <= root.Value && root.Value <= topBorder && IsBinaryTree(root.Left, root.Value - 1, root.Value + 1))
        {
            return true;
        }
        else if (root.Left != null && (root.Left.Value > root.Value || root.Value > topBorder || !IsBinaryTree(root.Left, root.Value - 1, root.Value + 1)))
        {
            return false;
        }

        if (root.Right != null && root.Right.Value > root.Value && root.Value >= bottomBorder && IsBinaryTree(root.Right, root.Value - 1, root.Value + 1))
        {
            return true;
        }
        else if (root.Right != null && (root.Right.Value <= root.Value || root.Value >= bottomBorder || !IsBinaryTree(root.Right, root.Value - 1, root.Value + 1)))
        {
            return false;
        }

        return true;
    }
}
