﻿namespace Day06_Trees;
public class BinaryTreeNode
{
    public BinaryTreeNode(int value, BinaryTreeNode left, BinaryTreeNode right)
    {
        Value = value;
        Left = left;
        Right = right;
    }
    public BinaryTreeNode(int value)
    {
        Value = value;
        Left = null;
        Right = null;
    }

    public int Value { get; set; }
    public BinaryTreeNode Left { get; set; }
    public BinaryTreeNode Right { get; set; }
}
