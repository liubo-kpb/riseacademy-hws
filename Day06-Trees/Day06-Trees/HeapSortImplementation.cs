﻿namespace Day06_Trees;

public class HeapSortImplementation
{
    //Formula 2*i+1
    public static List<int> HeapSort(int[] input)
    {
        if (input.Length == 0)
        {
            throw new ArgumentException("Collection is empty!");
        }

        PriorityQueue<int, int> elements = new PriorityQueue<int, int>();
        foreach (var number in input)
        {
            elements.Enqueue(number, number);
        }

        List<int> sortedElements = new List<int>();
        while (elements.Count > 0)
        {
            sortedElements.Add(elements.Dequeue());
        }

        return sortedElements;
    }
}
