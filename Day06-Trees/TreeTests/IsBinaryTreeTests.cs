﻿namespace TreeTests;
using Day06_Trees;
[TestClass]
public class TestCheckBinaryTree
{
    [TestMethod]
    public void TestIsBinaryTree()
    {
        //Assemnle
        BinaryTreeNode root = InitializeTree();

        //Act
        bool result = CheckBinaryTree.IsBinaryTree(root, root.Value + 1, root.Value + 1);

        //Assert
        Assert.IsTrue(result);
    }

    [TestMethod]
    public void TestIsNotBinaryTree()
    {
        //Assemnle
        BinaryTreeNode root = InitializeNonBinaryTree();

        //Act
        bool result = CheckBinaryTree.IsBinaryTree(root, root.Value - 1, root.Value + 1);

        //Assert
        Assert.IsFalse(result);
    }

    private BinaryTreeNode InitializeTree()
    {

        BinaryTreeNode node24 = new BinaryTreeNode(24);
        BinaryTreeNode node11 = new BinaryTreeNode(11, null, node24);


        BinaryTreeNode node35 = new BinaryTreeNode(35);
        BinaryTreeNode node38 = new BinaryTreeNode(38, node35, null);

        BinaryTreeNode node61 = new BinaryTreeNode(61);
        BinaryTreeNode node76 = new BinaryTreeNode(76);
        BinaryTreeNode node69 = new BinaryTreeNode(69, node61, node76);

        BinaryTreeNode node52 = new BinaryTreeNode(52, node38, node69);

        BinaryTreeNode rootNode = new BinaryTreeNode(25, node11, node52);

        return rootNode;
    }

    private BinaryTreeNode InitializeNonBinaryTree()
    {

        BinaryTreeNode node10 = new BinaryTreeNode(10);
        BinaryTreeNode node11 = new BinaryTreeNode(11, null, node10);


        BinaryTreeNode node35 = new BinaryTreeNode(35);
        BinaryTreeNode node38 = new BinaryTreeNode(38, node35, null);

        BinaryTreeNode node61 = new BinaryTreeNode(61);
        BinaryTreeNode node76 = new BinaryTreeNode(76);
        BinaryTreeNode node69 = new BinaryTreeNode(69, node61, node76);

        BinaryTreeNode node52 = new BinaryTreeNode(52, node38, node69);

        BinaryTreeNode rootNode = new BinaryTreeNode(25, node11, node52);

        return rootNode;
    }
}
