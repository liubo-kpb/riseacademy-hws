﻿namespace TreeTests;

using Day06_Trees;

[TestClass]
public class HeapSortTests
{
    [TestMethod]
    public void TestHeapSort()
    {
        //Assemble
        int[] numbers = { 100, 95, 72, 217, 19, 3, 15, 13, 14 };
        List<int> expected = new List<int>(numbers);
        expected.Sort();

        //Act
        List<int> result = HeapSortImplementation.HeapSort(numbers);
        
        //Assert
        CollectionAssert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestHeapSortWhenInputNull()
    {
        //Assemble
        int[] numbers = new int[0];
        string expected = "Collection is empty!";

        //Assert
        Assert.ThrowsException<ArgumentException>(() => HeapSortImplementation.HeapSort(numbers), expected);
    }
}
