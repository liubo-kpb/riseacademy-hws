﻿namespace TreeTests;

using Day06_Trees;

[TestClass]
public class MergeListsTests
{
    [TestMethod]
    public void TestMerging()
    {
        //Assemble
        int[,] lists = { { 1,2,3,4},
                         { 6,10,11,12},
                         { 5,7,8,9} };
        int[] expected = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

        //Act
        int[] result = MergeLists.MergeListsFunc(lists);

        //
        CollectionAssert.AreEquivalent(expected, result );
    }
}
