namespace TreeTests;

using Day06_Trees;
using System.Text;

[TestClass]
public class OrderTests
{
    [TestMethod]
    public void TestPreOrder()
    {
        //Assemble
        BinaryTreeNode root = InitializeTree();
        string expected = "25, 11, 24, 52, 38, 35, 69, 61, 76";

        //Act
        string result = GetOrders.PreOrder(root);

        //Assert
        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestPostOrder()
    {
        //Assemble
        BinaryTreeNode root = InitializeTree();
        string expected = "24, 11, 35, 38, 61, 76, 69, 52, 25";

        //Act
        string result = GetOrders.PostOrder(root);

        //Assert
        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestInOrder()
    {
        //Assemble
        BinaryTreeNode root = InitializeTree();
        string expected = "11, 24, 25, 35, 38, 52, 61, 69, 76";

        //Act
        string result = GetOrders.InOrder(root);

        //Assert
        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestGetTreeHeight()
    {
        //Assemble
        BinaryTreeNode root = InitializeTree();
        int expected = 3;

        //Act
        int result = TreeHeight.GetTreeHeight(root);

        //Assert
        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestOrderWithStreamWriter()
    {
        // arrange
        BinaryTreeNode root = InitializeTree();
        using (var stream = new MemoryStream())
        using (var writer = new StreamWriter(stream))
        {
            // act
            GetOrders.SWInOrder(root, writer);
            writer.Flush();

            // assert
            string actual = Encoding.UTF8.GetString(stream.ToArray());
            Assert.AreEqual("112425353852616976", actual);
        }
    }

    private BinaryTreeNode InitializeTree()
    {

        BinaryTreeNode node24 = new BinaryTreeNode(24);
        BinaryTreeNode node11 = new BinaryTreeNode(11, null, node24);


        BinaryTreeNode node35 = new BinaryTreeNode(35);
        BinaryTreeNode node38 = new BinaryTreeNode(38, node35, null);

        BinaryTreeNode node61 = new BinaryTreeNode(61);
        BinaryTreeNode node76 = new BinaryTreeNode(76);
        BinaryTreeNode node69 = new BinaryTreeNode(69, node61, node76);

        BinaryTreeNode node52 = new BinaryTreeNode(52, node38, node69);

        BinaryTreeNode rootNode = new BinaryTreeNode(25, node11, node52);

        return rootNode;
    }
}