﻿namespace Day11_OOP_E_I.Model.Animals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;
using Day11_OOP_E_I.Model.IO;

public abstract class Animal : IEatable
{
    private readonly int maxEnergy;
    private readonly int maxNutrition;
    protected int growthBorder;
    private Biome currentBiome;

    public Animal(string name, int maxEnergy, int maxNutrient, Biome biome, FoodEnum type)
    {
        if (maxNutrient % 10 == 0)
        {
            maxNutrient += 5;
        }

        Name = name;
        this.maxEnergy = maxEnergy;
        Energy = maxEnergy;
        this.maxNutrition = maxNutrient;
        CurrentNutrient = maxNutrient;
        IsAlive = true;
        Diet = new HashSet<FoodEnum>();
        Eating = new Behaviour("", "", $"{Name} is eating");
        Starving = new Behaviour("", "", $"{Name} is starving");
        Dying = new Behaviour("", "", $"{Name} has died.");
        Migrating = new Behaviour("", "", $"{Name} has migrated.");
        currentBiome = biome;
        Type = type;

    }

    public string Name { get; private set; }
    public FoodEnum Type { get; set; }
    public int LifeSpan { get; protected set; }
    public bool IsAlive { get; protected set; }
    public int CurrentNutrient { get; set; }
    public int RequiredNutrition => maxEnergy - Energy;
    public string FoodType => "Animal";
    public bool IsPlant => false;
    protected HashSet<FoodEnum> Diet { get; private set; }
    protected int Age { get; set; }
    private int Energy { get; set; }
    public bool IsStarving => Energy < maxEnergy * 0.3;
    private Behaviour Dying { get; }
    private Behaviour Starving { get; }
    private Behaviour Eating { get; }
    public Behaviour Migrating { get; }

    public Behaviour Feed(IEatable food)
    {
        Behaviour behaviour = Eating;

        if (Diet.Contains(food.Type) && food.CurrentNutrient > 0)
        {
            int providedNutrients = food.ProvideNutrient(this.RequiredNutrition);

            if (Energy < maxEnergy && Energy + providedNutrients <= maxEnergy)
            {
                Energy += providedNutrients;
            }
            else
            {
                Energy = maxEnergy;
            }
        }
        else
        {
            Energy--;
            behaviour = Starving;
            //Console.Beep();

            if (Energy == 0)
            {
                return Die();
            }
        }

        LifeSpan++;

        if (IsStarving)
        {
            behaviour = Starving;
        }
        if (LifeSpan > growthBorder && IsAlive)
        {
            Grow();
            behaviour.HasGrown = true;
        }

        return behaviour;
    }

    public Behaviour Die()
    {
        IsAlive = false;
        return Dying;
    }

    public int ProvideNutrient(int requiredNutrition)
    {
        int suplement = CurrentNutrient;
        if (requiredNutrition < CurrentNutrient)
        {
            suplement = requiredNutrition;
            CurrentNutrient -= suplement;
        }
        else
        {
            CurrentNutrient = 0;
        }

        IsAlive = false;

        return suplement;
    }

    public bool Move(IReadOnlyCollection<Biome> biomes)
    {
        bool hasMoved = false;
        foreach (var biome in biomes)
        {
            if (biome != currentBiome && biome.Habitat.Contains(Type))
            {
                biome.Animals.Add(this);
                currentBiome.Animals.Remove(this);
                currentBiome = biome;
                hasMoved = true;
                break;
            }
        }

        return hasMoved;
    }

    protected abstract string Grow();
}