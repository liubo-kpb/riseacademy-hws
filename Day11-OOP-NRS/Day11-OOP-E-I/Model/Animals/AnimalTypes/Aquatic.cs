﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;
using Day11_OOP_E_I.Model.IO;

public class Aquatic : Animal
{
    public Aquatic(string name, int maxEnergy, int maxNutrient, Biome biome, FoodEnum type) : base(name, maxEnergy, maxNutrient, biome, type)
    {
    }

    protected override string Grow() => "";
}
