﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;
using Day11_OOP_E_I.Model.IO;

public class Herbivorous : Animal
{
    public Herbivorous(string name, int maxEnergy, int maxNutrient, Biome biome, FoodEnum type) : base(name, maxEnergy, maxNutrient, biome, type)
    {
        growthBorder = 5;
        Diet.Add(FoodEnum.Carrot);
        Diet.Add(FoodEnum.Berry);
        Diet.Add(FoodEnum.Nut);
    }

    protected override string Grow()
    {
        if (LifeSpan > 545)
        {
            IsAlive = false;
            return $"{Name} died of old age";
        }

        if (LifeSpan > 15)
        {
            Diet.Add(FoodEnum.Cabbage);
            Diet.Add(FoodEnum.TallerPlant);
        }
        else if (LifeSpan > 10)
        {
            Diet.Add(FoodEnum.Herb);
        }
        else if (LifeSpan > 5)
        {
            Diet.Add(FoodEnum.Plant);
        }

        growthBorder += 5;
        Age++;

        return $"{Name} became {Age} years old";
    }
}
