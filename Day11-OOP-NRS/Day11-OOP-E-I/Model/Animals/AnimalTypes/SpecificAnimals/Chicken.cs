﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Chicken : Herbivorous
{
    private const int maxEnergy = 10;
    private const int maxNutrient = 8;

    public Chicken(string name, Biome biome) : base(name, maxEnergy, maxNutrient, biome, FoodEnum.Chicken)
    {
        Diet.Add(FoodEnum.Herb);
        Diet.Add(FoodEnum.Plant);
    }
}
