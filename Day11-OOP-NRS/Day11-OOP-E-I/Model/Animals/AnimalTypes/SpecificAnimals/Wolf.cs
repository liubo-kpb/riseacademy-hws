﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Wolf : Carnivorous
{
    private const int maxEnergy = 53;
    private const int maxNutrient = 33;
    public Wolf(string name, Biome biome) : base(name, maxEnergy, maxNutrient, biome, FoodEnum.Wolf) { }
}
