﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Beetle : Animal
{
    private const int maxEnergy = 6;
    private const int maxNutrient = 2;

    public Beetle(string name, Biome biome) : base(name, maxEnergy, maxNutrient, biome, FoodEnum.Beetle)
    {
        Diet.Add(FoodEnum.Herb);
        Diet.Add(FoodEnum.Plant);
    }

    protected override string Grow() => "";
}
