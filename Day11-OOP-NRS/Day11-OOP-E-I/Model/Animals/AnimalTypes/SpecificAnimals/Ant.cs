﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Ant : Animal
{
    private const int maxEnergy = 4;
    private const int maxNutrient = 1;
    public Ant(string name, Biome biome) : base(name, maxEnergy, maxNutrient, biome, FoodEnum.Ant)
    {
        Diet.Add(FoodEnum.Herb);
        Diet.Add(FoodEnum.Plant);
    }

    protected override string Grow() => "";
}
