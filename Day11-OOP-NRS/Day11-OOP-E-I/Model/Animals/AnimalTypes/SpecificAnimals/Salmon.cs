﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Salmon : Fish
{
    public Salmon(string name, Biome biome) : base(name, biome, FoodEnum.Salmon) { }
}
