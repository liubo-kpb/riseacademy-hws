﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Squirrel : Herbivorous
{

    private const int maxEnergy = 28;
    private const int maxNutrient = 19;
    public Squirrel(string name, Biome biome) : base(name, maxEnergy, maxNutrient, biome, FoodEnum.Squirrel) { }
}
