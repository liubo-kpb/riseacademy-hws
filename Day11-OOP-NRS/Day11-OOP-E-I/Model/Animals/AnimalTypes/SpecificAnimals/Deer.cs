﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Deer : Herbivorous
{
    private const int maxEnergy = 36;
    private const int maxNutrient = 18;

    public Deer(string name, Biome biome) : base(name, maxEnergy, maxNutrient, biome, FoodEnum.Deer)
    {
        Diet.Add(FoodEnum.Herb);
        Diet.Add(FoodEnum.Plant);
        Diet.Add(FoodEnum.TallerPlant);
        Diet.Add(FoodEnum.Grass);
    }
}
