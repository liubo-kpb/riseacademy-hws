﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class WildCat : Carnivorous
{
    private const int maxEnergy = 55;
    private const int maxNutrent = 33;
    public WildCat(string name, Biome biome) : base(name, maxEnergy, maxNutrent, biome, FoodEnum.WildCat) { }
}
