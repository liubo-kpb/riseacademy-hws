﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Fish : Aquatic
{
    private const int maxEnergy = 25;
    private const int maxNutrient = 15;

    public Fish(string name, Biome biome, FoodEnum type = FoodEnum.Fish) : base(name, maxEnergy, maxNutrient, biome, type)
    {
        growthBorder = 2;
        Diet.Add(FoodEnum.Ant);
        Diet.Add(FoodEnum.Herb);
        Diet.Add(FoodEnum.Plant);
    }

    protected override string Grow()
    {
        if (LifeSpan > 80)
        {
            IsAlive = false;
            return $"{Name} died of old age";
        }

        if (LifeSpan > 6)
        {
            Diet.Add(FoodEnum.Fish);
        }
        else if (LifeSpan > 4)
        {
            Diet.Add(FoodEnum.Worm);
        }
        else
        {
            Diet.Add(FoodEnum.Beetle);
        }

        growthBorder += 2;
        Age++;

        return $"{Name} became {Age} years old";
    }
}
