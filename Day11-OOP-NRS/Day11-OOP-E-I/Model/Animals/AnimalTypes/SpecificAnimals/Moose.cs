﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Moose : Herbivorous
{
    private const int maxEnergy = 38;
    private const int maxNutrient = 48;
    
    public Moose(string name, Biome biome) : base(name, maxEnergy, maxNutrient, biome, FoodEnum.Moose)
    {
        Diet.Add(FoodEnum.Herb);
        Diet.Add(FoodEnum.Plant);
        Diet.Add(FoodEnum.TallerPlant);
        Diet.Add(FoodEnum.Grass);
    }
}
