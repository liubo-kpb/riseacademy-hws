﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Krill : Aquatic
{
    private const int maxEnergy = 13;
    private const int maxNutrient = 15;
    public Krill(string name, Biome biome) : base(name, maxEnergy, maxNutrient, biome, FoodEnum.Krill)
    {
        Diet.Add(FoodEnum.Herb);
        Diet.Add(FoodEnum.Plant);
    }
}
