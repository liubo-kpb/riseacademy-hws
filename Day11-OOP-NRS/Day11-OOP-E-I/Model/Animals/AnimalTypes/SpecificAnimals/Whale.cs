﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Whale : Aquatic
{
    private const int maxEnergy = 55;
    private const int maxNutrient = 155;
    public Whale(string name, Biome biome) : base(name, maxEnergy, maxNutrient, biome, FoodEnum.Whale)
    {
        growthBorder = 50;
        Diet.Add(FoodEnum.Fish);
        Diet.Add(FoodEnum.Salmon);
        Diet.Add(FoodEnum.Krill);
        Diet.Add(FoodEnum.Squid);
        Diet.Add(FoodEnum.Pork);
        Diet.Add(FoodEnum.Chicken);
        Diet.Add(FoodEnum.Plant);
        Diet.Add(FoodEnum.TallerPlant);
        Diet.Add(FoodEnum.Herb);
    }

    protected override string Grow()
    {
        if (LifeSpan > 150)
        {
            IsAlive = false;
            return $"{Name} died of old age";
        }

        growthBorder += 30;
        Age++;

        return "";
    }
}
