﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Squid : Aquatic
{
    private const int maxEnergy = 10;
    private const int maxNutrient = 8;

    public Squid(string name, Biome biome) : base(name, maxEnergy, maxNutrient, biome, FoodEnum.Squid)
    {
        growthBorder = 6;
        Diet.Add(FoodEnum.Herb);
        Diet.Add(FoodEnum.Plant);
    }

    protected override string Grow()
    {
        if (LifeSpan > 120)
        {
            IsAlive = false;
            return $"{Name} died of old age";
        }

        if (LifeSpan > 6)
        {
            Diet.Add(FoodEnum.Fish);
        }
        else if (LifeSpan > 4)
        {
            Diet.Add(FoodEnum.Worm);
        }
        else
        {
            Diet.Add(FoodEnum.Beetle);
        }

        growthBorder += 6;
        Age++;

        return $"{Name} became {Age} years old";
    }
}
