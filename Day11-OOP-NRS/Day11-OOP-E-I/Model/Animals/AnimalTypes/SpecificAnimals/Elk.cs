﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Elk : Herbivorous
{
    private const int maxEnergy = 16;
    private const int maxNutrient = 28;

    public Elk(string name, Biome biome) : base(name, maxEnergy, maxNutrient, biome, FoodEnum.Elk)
    {
        Diet.Add(FoodEnum.Herb);
        Diet.Add(FoodEnum.Plant);
        Diet.Add(FoodEnum.TallerPlant);
        Diet.Add(FoodEnum.Grass);
    }
}
