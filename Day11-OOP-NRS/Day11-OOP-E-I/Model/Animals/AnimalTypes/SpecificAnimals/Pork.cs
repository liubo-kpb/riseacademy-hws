﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Pork : Herbivorous
{
    private const int maxEnergy = 17;
    private const int maxNutrient = 14;

    public Pork(string name, Biome biome) : base(name, maxEnergy, maxNutrient, biome, FoodEnum.Pork)
    {
        Diet.Add(FoodEnum.Herb);
        Diet.Add(FoodEnum.Plant);
    }
}
