﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;

public class Rabbit : Herbivorous
{
    private const int maxEnergy = 27;
    private const int maxNutrient = 27;
    public Rabbit(string name, Biome biome) : base(name, maxEnergy, maxNutrient, biome, FoodEnum.Rabbit)
    {
    }
}
