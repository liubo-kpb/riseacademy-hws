﻿namespace Day11_OOP_E_I.Model.Animals.AnimalTypes;

using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;
using Day11_OOP_E_I.Model.IO;

public class Carnivorous : Animal
{
    public Carnivorous(string name, int maxEnergy, int maxNutrient, Biome biome, FoodEnum type ) : base(name, maxEnergy, maxNutrient, biome, type)
    {
        growthBorder = 4;
        Diet.Add(FoodEnum.Squirrel);
        Diet.Add(FoodEnum.Berry);
        Diet.Add(FoodEnum.Fish);
        Diet.Add(FoodEnum.Pork);
        Diet.Add(FoodEnum.Chicken);
    }

    protected override string Grow()
    {
        if (LifeSpan >= 54)
        {
            IsAlive = false; // dies of old age
            return $"{Name} died of old age";
        }

        if (LifeSpan > 12)
        {
            Diet.Add(FoodEnum.Elk);
            Diet.Add(FoodEnum.Moose);
        }
        else if (LifeSpan > 8)
        {
            Diet.Add(FoodEnum.Deer);
        }
        else if (LifeSpan > 4)
        {
            Diet.Add(FoodEnum.Rabbit);
        }

        growthBorder += 4;
        Age++;

        return $"{Name} became {Age} years old";
    }
}
