﻿namespace Day11_OOP_E_I.Model.IO;

public class ConsoleInteractor : IInteraction
{
    public void Invoke(Behaviour behaviour, bool enabled)
    {
        if (enabled)
        {
            string message = behaviour.TextExpression;

            if (behaviour.HasGrown)
            {
                message += " and grew.";
            }

            Console.WriteLine(message);
        }
    }
}
