﻿namespace Day11_OOP_E_I.Model.IO;

public class Behaviour
{
    public Behaviour(string soundFilePath, string animationFilePath, string textExpression)
    {
        SoundFilePath = soundFilePath;
        AnimationFilePath = animationFilePath;
        TextExpression = textExpression;
    }

    public string SoundFilePath { get; } // these are just concept ideas for holding all the information required for a UI. So file paths won't be used in this case.
    public string AnimationFilePath { get; }
    public string TextExpression { get; } // this will probably be the only property that will actually be used.
    public bool HasGrown { get; set; }
}