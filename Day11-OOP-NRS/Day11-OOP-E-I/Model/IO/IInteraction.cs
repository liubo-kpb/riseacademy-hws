﻿namespace Day11_OOP_E_I.Model.IO;


public interface IInteraction
{
    public void Invoke(Behaviour behaviour, bool enabled);
}
