﻿namespace Day11_OOP_E_I.Model.IO;

public class UserInterface : IInteraction
{
    public void Invoke(Behaviour behaviour, bool enabled)
    {
        if (enabled)
        {
            string soundFilePath = behaviour.SoundFilePath;
            string animationFilePath = behaviour.AnimationFilePath;
            if (behaviour.HasGrown)
            {
                //Increase animal icon to simulate growth.
            }

            //Imagine that we are calling and using a UI below with the above mentioned files.
        }
    }
}
