﻿namespace Day11_OOP_E_I.Model.IO;

public class FileInteractor : IInteraction
{
    public void Invoke(Behaviour behaviour, bool enabled)
    {
        if (enabled)
        {
            string outputFilePath = "output.txt";
            using StreamWriter writer = new StreamWriter(outputFilePath);
            string message = behaviour.TextExpression;

            if (behaviour.HasGrown)
            {
                message += " and grew.";
            }

            writer.WriteLine(message);
        }
    }
}
