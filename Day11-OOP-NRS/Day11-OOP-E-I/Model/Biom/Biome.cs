﻿namespace Day11_OOP_E_I.Model.Biom;

using Day11_OOP_E_I.Model.Animals;
using Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;
using Day11_OOP_E_I.Model.Foods;
using Day11_OOP_E_I.Model.Foods.Plants;

public abstract class Biome
{
    public List<IEatable> Food { get; set; }
    public List<Animal> Animals { get; set; }
    public HashSet<FoodEnum> Habitat { get; set; }

    protected void CreateLife()
    {
        Random random = new Random();
        int elementsInBiome = random.Next(Habitat.Count * 2, Habitat.Count * 3); // Random choose number of elements in simulation

        for (int i = 0; i < elementsInBiome; i++)
        {
            int elementToCreate = random.Next(0, 24); // 23 overall possible objects
            IEatable creation = null;

            if (Habitat.Contains((FoodEnum) elementToCreate))
            {

                switch (elementToCreate)
                {
                    case 0:
                        {
                            creation = new Ant($"Ant {i + 1}", this);
                            break;
                        }
                    case 1:
                        {
                            creation = new Beetle($"Beetle {i + 1}", this);
                            break;
                        }
                    case 2:
                        {
                            creation = new Chicken($"Chicken {i + 1}", this);
                            break;
                        }
                    case 3:
                        {
                            creation = new Deer($"Deer {i + 1}", this);
                            break;
                        }
                    case 4:
                        {
                            creation = new Elk($"Elk {i + 1}", this);
                            break;
                        }
                    case 5:
                        {
                            creation = new Fish($"Fish {i + 1}", this);
                            break;
                        }
                    case 6:
                        {
                            creation = new Krill($"Krill {i + 1}", this);
                            break;
                        }
                    case 7:
                        {
                            creation = new Moose($"Moose {i + 1}", this);
                            break;
                        }
                    case 8:
                        {
                            creation = new Pork($"Pork {i + 1}", this);
                            break;
                        }
                    case 9:
                        {
                            creation = new Rabbit($"Rabbit {i + 1}", this);
                            break;
                        }
                    case 10:
                        {
                            creation = new Salmon($"Salmon {i + 1}", this);
                            break;
                        }
                    case 11:
                        {
                            creation = new Squid($"Squid {i + 1}", this);
                            break;
                        }
                    case 12:
                        {
                            creation = new Squirrel($"Squirrel {i + 1}", this);
                            break;
                        }
                    case 13:
                        {
                            creation = new Whale($"Whale {i + 1}", this);
                            break;
                        }
                    case 14:
                        {
                            creation = new WildCat($"Wildcat {i + 1}", this);
                            break;
                        }
                    case 15:
                        {
                            creation = new Wolf($"Wolf {i + 1}", this);
                            break;
                        }
                    case 16:
                        {
                            creation = new Berry();
                            break;
                        }
                    case 17:
                        {
                            creation = new Cabbage();
                            break;
                        }
                    case 18:
                        {
                            creation = new Grass();
                            break;
                        }
                    case 19:
                        {
                            creation = new Herb();
                            break;
                        }
                    case 20:
                        {
                            creation = new Nut();
                            break;
                        }
                    case 21:
                        {
                            creation = new PoisonBerry();
                            break;
                        }
                    case 22:
                        {
                            creation = new PoisonMushroom();
                            break;
                        }
                    case 23:
                        {
                            creation = new TallerPlant();
                            break;
                        }
                }

                Food.Add(creation);
                if (!creation.IsPlant)
                {
                    Animals.Add(creation as Animal);
                }
            }
            else
            {
                i--;
            }
        }

    }
}