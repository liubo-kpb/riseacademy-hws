﻿namespace Day11_OOP_E_I.Model.Biom;

using Day11_OOP_E_I.Model.Biom.BiomeTypes;

public class Map : IMap
{
    private HashSet<Biome> biomes = new HashSet<Biome>();
    public Map() => CreateTiles();

    public IReadOnlyCollection<Biome> Biomes => biomes;

    private void CreateTiles()
    {
        Random random = new Random();
        int tilesToCreate = random.Next(3, 11);

        for (int i = 0; i < tilesToCreate; i++)
        {
            int tileCase = random.Next(8);
            Biome biome = null;

            switch (tileCase)
            {
                case 0:
                    {
                        biome = new Desert();
                        break;
                    }
                case 1:
                    {
                        biome = new Forest();
                        break;
                    }
                case 2:
                    {
                        biome = new Jungle();
                        break;
                    }
                case 3:
                    {
                        biome = new Mountain();
                        break;
                    }
                case 4:
                    {
                        biome = new Ocean();
                        break;
                    }
                case 5:
                    {
                        biome = new Plane();
                        break;
                    }
                case 6:
                    {
                        biome = new River();
                        break;
                    }
                case 7:
                    {
                        biome = new See();
                        break;
                    }
            }

            biomes.Add(biome);
        }
    }
}