﻿namespace Day11_OOP_E_I.Model.Biom.BiomeTypes;
using Day11_OOP_E_I.Model.Animals;

using Day11_OOP_E_I.Model.Foods;
using System;

public class Desert : Biome
{
    public Desert()
    {
        Food = new List<IEatable>();
        Animals = new List<Animal>();
        Habitat = new HashSet<FoodEnum>()
        {
            FoodEnum.Ant,
            FoodEnum.Beetle,
            FoodEnum.Herb,
            FoodEnum.TallerPlant,
            FoodEnum.Plant
        };

        CreateLife();
    }
}
