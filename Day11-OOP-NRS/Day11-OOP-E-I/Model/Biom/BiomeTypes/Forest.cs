﻿namespace Day11_OOP_E_I.Model.Biom.BiomeTypes;

using Day11_OOP_E_I.Model.Animals;
using Day11_OOP_E_I.Model.Foods;

public class Forest : Biome
{
    public Forest()
    {
        Food = new List<IEatable>();
        Animals = new List<Animal>();
        Habitat = new HashSet<FoodEnum>()
        {
            FoodEnum.Ant,
            FoodEnum.Beetle,
            FoodEnum.Deer,
            FoodEnum.Elk,
            FoodEnum.Moose,
            FoodEnum.Rabbit,
            FoodEnum.Wolf,
            FoodEnum.WildCat,
            FoodEnum.Squirrel,
            FoodEnum.Herb,
            FoodEnum.Berry,
            FoodEnum.TallerPlant,
            FoodEnum.PoisonBerry,
            FoodEnum.Grass,
            FoodEnum.Nut
        };

        CreateLife();
    }
}
