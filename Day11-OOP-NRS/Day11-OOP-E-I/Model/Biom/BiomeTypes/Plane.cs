﻿namespace Day11_OOP_E_I.Model.Biom.BiomeTypes;

using Day11_OOP_E_I.Model.Animals;
using Day11_OOP_E_I.Model.Foods;

public class Plane : Biome
{
    public Plane()
    {
        Food = new List<IEatable>();
        Animals = new List<Animal>();
        Habitat = new HashSet<FoodEnum>()
        {
            FoodEnum.Ant,
            FoodEnum.Beetle,
            FoodEnum.Deer,
            FoodEnum.Elk,
            FoodEnum.Moose,
            FoodEnum.Rabbit,
            FoodEnum.Wolf,
            FoodEnum.WildCat,
            FoodEnum.Herb,
            FoodEnum.Berry,
            FoodEnum.PoisonBerry,
            FoodEnum.Grass,
            FoodEnum.Cabbage
        };

        CreateLife();
    }
}
