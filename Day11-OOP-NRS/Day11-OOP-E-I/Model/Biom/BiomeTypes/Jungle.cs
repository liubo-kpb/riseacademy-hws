﻿namespace Day11_OOP_E_I.Model.Biom.BiomeTypes;

using Day11_OOP_E_I.Model.Animals;
using Day11_OOP_E_I.Model.Foods;

public class Jungle : Biome
{
    public Jungle()
    {
        Food = new List<IEatable>();
        Animals = new List<Animal>();
        Habitat = new HashSet<FoodEnum>()
        {
            FoodEnum.WildCat,
            FoodEnum.Ant,
            FoodEnum.Beetle,
            FoodEnum.Rabbit,
            FoodEnum.Squirrel,
            FoodEnum.Plant,
            FoodEnum.TallerPlant,
            FoodEnum.Grass,
            FoodEnum.Cabbage,
            FoodEnum.Nut,
            FoodEnum.Herb
        };

        CreateLife();
    }
}
