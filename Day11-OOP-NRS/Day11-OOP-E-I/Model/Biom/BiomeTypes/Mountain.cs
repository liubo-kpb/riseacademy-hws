﻿namespace Day11_OOP_E_I.Model.Biom.BiomeTypes;

using Day11_OOP_E_I.Model.Animals;
using Day11_OOP_E_I.Model.Foods;

public class Mountain : Biome
{
    public Mountain()
    {
        Food = new List<IEatable>();
        Animals = new List<Animal>();
        Habitat = new HashSet<FoodEnum>()
        {
            FoodEnum.Ant,
            FoodEnum.Beetle,
            FoodEnum.Deer,
            FoodEnum.Elk,
            FoodEnum.Moose,
            FoodEnum.Rabbit,
            FoodEnum.Wolf,
            FoodEnum.WildCat,
            FoodEnum.Squirrel,
            FoodEnum.Herb,
            FoodEnum.Berry,
            FoodEnum.TallerPlant,
            FoodEnum.PoisonBerry,
            FoodEnum.PoisonMushroom,
            FoodEnum.Grass,
            FoodEnum.Cabbage,
            FoodEnum.Nut
        };

        CreateLife();
    }
}
