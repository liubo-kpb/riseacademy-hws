﻿namespace Day11_OOP_E_I.Model.Biom.BiomeTypes;

using Day11_OOP_E_I.Model.Animals;
using Day11_OOP_E_I.Model.Foods;

public class See : Biome
{
    public See()
    {
        Food = new List<IEatable>();
        Animals = new List<Animal>();
        Habitat = new HashSet<FoodEnum>()
        {
            FoodEnum.Fish,
            FoodEnum.Salmon,
            FoodEnum.Herb,
            FoodEnum.Plant
        };

        CreateLife();
    }
}
