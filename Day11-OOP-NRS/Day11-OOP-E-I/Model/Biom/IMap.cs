﻿namespace Day11_OOP_E_I.Model.Biom;
public interface IMap
{
    public IReadOnlyCollection<Biome> Biomes { get; }
}