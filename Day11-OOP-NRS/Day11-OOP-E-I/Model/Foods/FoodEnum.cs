﻿namespace Day11_OOP_E_I.Model.Foods;

public enum FoodEnum
{
    Ant,
    Beetle,
    Chicken,
    Deer,
    Elk,
    Fish,
    Krill,
    Moose,
    Pork,
    Rabbit,
    Salmon,
    Squid,
    Squirrel,
    Whale,
    WildCat,
    Wolf,
    Berry,
    Cabbage,
    Grass,
    Herb,
    Nut,
    Plant,
    PoisonBerry,
    PoisonMushroom,
    TallerPlant,
    Worm,
    Carrot
}
