﻿namespace Day11_OOP_E_I.Model.Foods.Plants;
public class PoisonMushroom : Plant
{
    private const int maxNutrient = 0;
    public PoisonMushroom() : base(maxNutrient, FoodEnum.PoisonMushroom)
    {
    }
}
