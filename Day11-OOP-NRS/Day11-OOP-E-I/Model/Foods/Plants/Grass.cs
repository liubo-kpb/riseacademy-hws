﻿namespace Day11_OOP_E_I.Model.Foods.Plants;

internal class Grass : Plant
{
    private const int maxNutrient = 5;
    public Grass() : base(maxNutrient, FoodEnum.Grass) { }
}
