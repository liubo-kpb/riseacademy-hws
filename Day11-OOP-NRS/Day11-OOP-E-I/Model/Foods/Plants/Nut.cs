﻿namespace Day11_OOP_E_I.Model.Foods.Plants;

public class Nut : Plant
{
    private const int maxNutrient = 3;
    public Nut() : base(maxNutrient, FoodEnum.Nut) { }
}