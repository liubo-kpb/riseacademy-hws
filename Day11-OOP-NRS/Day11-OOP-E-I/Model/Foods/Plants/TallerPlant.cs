﻿namespace Day11_OOP_E_I.Model.Foods.Plants;

internal class TallerPlant : Plant
{
    private const int maxNutrient = 13;
    public TallerPlant() : base(maxNutrient, FoodEnum.TallerPlant) { }
}
