﻿namespace Day11_OOP_E_I.Model.Foods.Plants;

public class Cabbage : Plant
{
    private const int maxNutrient = 6;
    public Cabbage() : base(maxNutrient, FoodEnum.Cabbage) { }
}
