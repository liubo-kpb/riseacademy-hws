﻿namespace Day11_OOP_E_I.Model.Foods.Plants;

internal class Herb : Plant
{
    private const int maxNutrient = 11;
    public Herb() : base(maxNutrient, FoodEnum.Herb) { }
}
