﻿namespace Day11_OOP_E_I.Model.Foods.Plants;


public class Berry : Plant
{
    private const int maxNutrient = 3;
    public Berry() : base(maxNutrient, FoodEnum.Berry) { }
}
