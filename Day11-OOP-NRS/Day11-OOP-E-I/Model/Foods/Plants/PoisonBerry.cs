﻿namespace Day11_OOP_E_I.Model.Foods.Plants;
public class PoisonBerry : Berry
{
    public PoisonBerry()
    {
        Type = FoodEnum.PoisonBerry;
    }
}
