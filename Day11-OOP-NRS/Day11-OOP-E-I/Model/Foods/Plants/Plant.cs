﻿namespace Day11_OOP_E_I.Model.Foods.Plants;

public class Plant : IPlant
{
    private readonly int maxNutrient;

    public Plant(int maxNutrient, FoodEnum type)
    {
        if (maxNutrient % 10 == 0)
        {
            maxNutrient += 5;
        }

        this.maxNutrient = maxNutrient;
        CurrentNutrient = maxNutrient;
        this.Type = type;

    }

    public FoodEnum Type { get; set; }

    public int CurrentNutrient { get; set; }

    public int NutrientCoeficient => maxNutrient % 10;

    public string FoodType => "Plant";

    public bool IsPlant => true;

    public int ProvideNutrient(int requiredNutrition)
    {
        int suplement = CurrentNutrient;
        if (requiredNutrition < CurrentNutrient)
        {
            suplement = requiredNutrition;
        }
        CurrentNutrient = 0;

        return suplement;
    }

    public void Regenerate()
    {
        if (CurrentNutrient + NutrientCoeficient > maxNutrient)
        {
            CurrentNutrient = maxNutrient;
        }
        else
        {
            CurrentNutrient += NutrientCoeficient;
        }
    }
}
