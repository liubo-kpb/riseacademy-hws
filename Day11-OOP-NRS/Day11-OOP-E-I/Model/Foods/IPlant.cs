﻿namespace Day11_OOP_E_I.Model.Foods;

internal interface IPlant : IEatable
{
    public void Regenerate();
}
