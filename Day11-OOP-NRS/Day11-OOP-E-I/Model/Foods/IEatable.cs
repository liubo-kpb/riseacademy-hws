﻿namespace Day11_OOP_E_I.Model.Foods;

public interface IEatable
{
    public int CurrentNutrient { get; set; }
    public string FoodType { get; }
    public bool IsPlant { get; }
    public int ProvideNutrient(int requiredNutrition);
    public FoodEnum Type { get; set; }
}
