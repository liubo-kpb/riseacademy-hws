﻿namespace Day11_OOP_E_I;

using Day11_OOP_E_I.Model.Animals;
using Day11_OOP_E_I.Model.Biom;
using Day11_OOP_E_I.Model.Foods;
using Day11_OOP_E_I.Model.IO;

public class Engine
{
    private bool showSimulationStatistic;
    private bool showDailyStatistic;
    private Map map = new Map();
    private IInteraction interactor = new ConsoleInteractor();
    private int day = 0;

    public Engine(bool showDailyStatistic, bool showSimulationStatistic)
    {
        this.showDailyStatistic = showDailyStatistic;
        this.showSimulationStatistic = showSimulationStatistic;

        StartSimulation();
    }

    private void StartSimulation()
    {
        while (map.Biomes.Any(b => b.Animals.Any(a => a.IsAlive)))
        {
            ++day;

            foreach (var biome in map.Biomes)
            {
                if (biome.Animals.Any(a => a.IsAlive))
                {
                    SimulateLife(biome);
                }
            }
        }

        if (showSimulationStatistic)
        {
            foreach (var biome in map.Biomes)
            {
                PringStatistic(biome);
            }
        }
    }

    private void SimulateLife(Biome biome)
    {
        List<Animal> animals = biome.Animals;
        List<IEatable> food = biome.Food;

        int deadAnimals = 0;

        foreach (IPlant plant in food.Where(f => f.IsPlant))
        {
            plant.Regenerate();
        }

        foreach (Animal animal in animals.ToList())
        {
            if (animal.IsAlive)
            {
                if (animal.IsStarving && animal.Move(map.Biomes))
                {
                    interactor.Invoke(animal.Migrating, showDailyStatistic);
                }

                Random rnd = new Random();
                int randomFood = rnd.Next(0, food.Count);

                interactor.Invoke(animal.Feed(food[randomFood]), showDailyStatistic);
            }
            else
            {
                deadAnimals++;
            }
        }

        if (showDailyStatistic)
        {
            Behaviour behaviour = new Behaviour("", "", $"{biome.GetType().Name}, day {day}:{Environment.NewLine} There are {animals.Count - deadAnimals} living animals. {deadAnimals} are dead.");
            interactor.Invoke(behaviour, showDailyStatistic);
        }
    }
    private void PringStatistic(Biome biome)
    {
        List<Animal> animals = biome.Animals;
        int possibleCreatures = 26; // current number of foods in FoodEnum

        for (int i = 0; i < possibleCreatures; i++)
        {
            FoodEnum food = (FoodEnum) i;
            if (animals.Any(a => a.Type == food))
            {
                var animalGroup = animals.Where(a => a.Type == food);
                string textExpression = $"Biome {biome.GetType().Name} had {animalGroup.Count()} {food.ToString()}s. They survived for a maximum of {animalGroup.Max(a => a.LifeSpan)}, a minimum of {animalGroup.Min(a => a.LifeSpan)} which makes an average life span of {animalGroup.Sum(a => a.LifeSpan) / animalGroup.Count()} days.";
                Behaviour overallStatistic = new Behaviour("path to sound file", "path to animation file", textExpression);
                interactor.Invoke(overallStatistic, showSimulationStatistic);
            }
        }
    }
}
