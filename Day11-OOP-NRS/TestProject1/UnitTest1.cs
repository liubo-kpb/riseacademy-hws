namespace TestProject1;

using Day11_OOP_E_I.Model.Animals.AnimalTypes.SpecificAnimals;
using Day11_OOP_E_I.Model.Foods;
using Day11_OOP_E_I.Model.Foods.Plants;

[TestClass]
public class UnitTest1
{
    //Wolf wolfE = new Wolf("Ernie");
    //Wolf wolfB = new Wolf("Beatris");

    //WildCat wcChochi = new WildCat("Chochi");
    //WildCat wcPuss = new WildCat("Puss");

    //Fish fishGoldy = new Fish("Goldy");
    //Fish fishWE = new Fish("WeidEyes");

    //Whale whaleH = new Whale("Hungry");
    //Whale whaleJ = new Whale("Joker");

    //Rabbit rabbitH = new Rabbit("Hair");
    //Rabbit rabbitF = new Rabbit("Fluffy");

    //Squirrel squirrelSH = new Squirrel("StickyHands");
    //Squirrel squirrelBT = new Squirrel("BrownTail");

    [TestMethod]
    public void TestWildCatEnergyDrain()
    {
        //Assemble
        Wolf wolfB = new Wolf("Beatris");

        wolfB.Feed(new Rabbit("r"));
        wolfB.Feed(new Cabbage());
        wolfB.Feed(new Fish("f"));
        wolfB.Feed(new Moose("m"));
        wolfB.Feed(new Nut());

        int expected = 48;

        //Act
        int result = wolfB.Energy;

        //Assert
        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestKillWolf()
    {
        //Assemble
        WildCat wcChochi = new WildCat("Chochi");

        //Act
        for (int i = 0; i < 56; i++)
        {
            wcChochi.Feed(new PoisonBerry());
        }

        //Assert
        Assert.IsFalse(wcChochi.IsAlive);
    }
}