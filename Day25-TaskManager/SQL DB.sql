CREATE DATABASE TasksDB

USE TasksDB

CREATE TABLE Assignments(
	Id INT PRIMARY KEY IDENTITY,
	CreatedBy VARCHAR(50) NOT NULL,
	AssignedTo VARCHAR(50)
);

CREATE TABLE Tasks(
	Id INT PRIMARY KEY IDENTITY,
	Name VARCHAR(50) NOT NULL,
	Description VARCHAR(100),
	CreatedOn DATETIME NOT NULL,
	Status BIT DEFAULT (0),
	AssignmentId INT FOREIGN KEY REFERENCES Assignments(Id)
);