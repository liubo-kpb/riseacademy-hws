﻿namespace TaskManager.Services;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TaskManager.Models;

public class TasksDBService : IDisposable
{
    public void Dispose()
    {

    }

    [HttpGet]
    public List<WorkTask> GetAllTasks()
    {
        List<WorkTask> tasks = new List<WorkTask>();
        using (TasksDBContext context = new TasksDBContext())
        {
            tasks = context.WorkTasks.Include(task => task.Assignment)
                                     .ToList();
        }

        return tasks;
    }

    [HttpGet]
    public void Create(WorkTask taskToAdd)
    {
        using (TasksDBContext context = new TasksDBContext())
        {
            context.WorkTasks.Add(taskToAdd);
            context.SaveChanges();
        }
    }

    [HttpPatch]
    public void Edit(WorkTask editedTask)
    {
        using (TasksDBContext context = new TasksDBContext())
        {
            var taskToEdit = context.WorkTasks.Include(task => task.Assignment).FirstOrDefault(t => t.Id == editedTask.Id);
            taskToEdit.Name = editedTask.Name;
            taskToEdit.Description = editedTask.Description;
            taskToEdit.CreatedOn = editedTask.CreatedOn;
            taskToEdit.Status = editedTask.Status;
            taskToEdit.Assignment.AssignedTo = editedTask.Assignment.AssignedTo;
            taskToEdit.Assignment.CreatedBy = editedTask.Assignment.CreatedBy;
            context.SaveChanges();
        }
    }

    [HttpGet]
    public WorkTask? Detail(int id)
    {
        WorkTask task = GetTask(id);

        using (TasksDBContext context = new TasksDBContext())
        {
            if (task != null)
            {
                return task;
            }
        }

        return null;
    }

    [HttpGet]
    public void Delete(int id)
    {
        WorkTask task = new WorkTask();
        using (TasksDBContext context = new TasksDBContext())
        {
            task = context.WorkTasks.Include(task => task.Assignment).FirstOrDefault(t => t.Id == id);
            context.WorkTasks.Remove(task);
            context.Assignments.Remove(task.Assignment);
            context.SaveChanges();
        }
    }

    [HttpGet]
    public WorkTask? GetTask(int id)
    {
        WorkTask task = new WorkTask();

        using (TasksDBContext context = new TasksDBContext())
        {
            task = context.WorkTasks.Include(task => task.Assignment).FirstOrDefault(t => t.Id == id);
        }

        return task;
    }
}
