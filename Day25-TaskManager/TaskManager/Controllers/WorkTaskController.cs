﻿namespace TaskManager.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TaskManager.Models;
using TaskManager.Services;

public class WorkTaskController : Controller
{
    public IActionResult Index()
    {
        List<WorkTask> tasks = new List<WorkTask>();
        using (TasksDBService service = new TasksDBService())
        {
            tasks = service.GetAllTasks();
        }

        if (tasks.Count > 0)
        {
            return View(tasks);
        }

        return View();
    }

    [HttpGet]
    public IActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public IActionResult Create(WorkTask task)
    {
        if (task == null)
        {
            return BadRequest();
        }

        WorkTask newTask = new WorkTask();
        newTask.Name = task.Name;
        newTask.Description = task.Description;
        newTask.CreatedOn = DateTime.Now;
        newTask.Status = task.Status;
        newTask.Assignment = new Assignment()
        {
            CreatedBy = task.Assignment.CreatedBy,
            AssignedTo = task.Assignment.AssignedTo
        };

        using (TasksDBService service = new TasksDBService())
        {
            service.Create(newTask);
        }

        return RedirectToAction("Index");
    }

    [HttpGet]
    public IActionResult Edit(int id)
    {
        using (TasksDBContext context = new TasksDBContext())
        {
            if (context.WorkTasks.Any(t => t.Id == id))
            {
                WorkTask task = new WorkTask();
                using (TasksDBService service = new TasksDBService())
                {
                    task = service.GetTask(id);
                }

                return View(task);
            }
        }

        return BadRequest();
    }

    [HttpPost]
    public IActionResult Edit(WorkTask editedTask)
    {
            using (TasksDBService service = new TasksDBService())
            {
                service.Edit(editedTask);
            }

        return RedirectToAction("Index");
    }

    [HttpGet]
    public IActionResult Details(int id)
    {
        WorkTask task = new WorkTask();
        using (TasksDBService service = new TasksDBService())
        {
            task = service.GetTask(id);
        }

        if (task == null)
        {
            return BadRequest();
        }

        return View(task);
    }

    [HttpGet]
    public IActionResult Delete(int id)
    {
        using (TasksDBService service = new TasksDBService())
        {
            service.Delete(id);
        }

        return RedirectToAction("Index");
    }
}