﻿using Day05_HashTables;

namespace HashTablesTests;

[TestClass]
public class Task06Tests
{
    [TestMethod]
    public void TestEncryption()
    {
        //Assign
        string username = "ltoshev";
        string password = "200323!Lt";
        string expected = "C1DF8C2D06BF425629A7F533C393AB43C138803C3C4AFD3C9156CA39E7B2EB5A";

        //Act
        string result = TaskSolutions.EncryptPasword(username, password);

        //Assert
        Assert.AreEqual(expected, result);
    }
}
