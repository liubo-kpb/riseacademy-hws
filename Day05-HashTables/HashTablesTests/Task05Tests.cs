﻿namespace HashTablesTests;
using Day05_HashTables;

[TestClass]
public class Task05Tests
{
    [TestMethod]
    public void TestGroupAnagrams()
    {
        //Assemble
        List<string> words = new List<string>(new string[] { "test", "phone", "sett", "estt", "nepho"});
        Dictionary<string, HashSet<string>> expectedDic = new Dictionary<string, HashSet<string>>();
        expectedDic["test"] = new HashSet<string>(new string[] {"sett", "estt" });
        expectedDic["phone"] = new HashSet<string>
        {
            "nepho"
        };
        string expected = Stringer(expectedDic);

        //Act
        string result = TaskSolutions.GroupByAnagrams(words);

        //Assert
        Assert.AreEqual(expected, result);
    }

    private string Stringer(Dictionary<string, HashSet<string>> dictionary)
    {
        var pairs = dictionary.Select(p => p.Key + ": " + string.Join(", ", p.Value));

        return string.Join(Environment.NewLine, pairs);
    }
}
