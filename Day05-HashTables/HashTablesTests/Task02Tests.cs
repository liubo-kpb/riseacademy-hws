﻿using Day05_HashTables;

namespace HashTablesTests;

[TestClass]
public class Task02Tests
{
    [TestMethod]
    public void FindAllRecuringElements()
    {
        //Assemble
        int[] array1 = { 5, 22, 21, 13, 97, 101, 102 };
        int[] array2 = { 102, 97, 98, 21, 5 };
        int[] expected = { 102, 97, 21, 5 };

        //Act
        int[] result = TaskSolutions.GetRecuringElements(array1, array2);

        //Assert
        CollectionAssert.AreEquivalent(expected, result);
    }

    [TestMethod]
    public void TestWithNoSameElements()
    {
        //Assemble
        int[] array1 = { 5, 22, 21, 13, 97, 101, 102 };
        int[] array2 = { 105, 55, 98, 23, 3 };
        int[] expected = new int[0];

        //Act
        int[] result = TaskSolutions.GetRecuringElements(array1, array2);

        //Assert
        CollectionAssert.AreEquivalent(expected, result);
    }

    [TestMethod]
    public void TestWithOneEmptyArray()
    {
        //Assemble
        int[] array1 = { 5, 22, 21, 13, 97, 101, 102 };
        int[] array2 = new int[0];
        string expected = "Both arrays must have values";

        //Assert
        Assert.ThrowsException<ArgumentNullException>(() => TaskSolutions.GetRecuringElements(array1, array2), expected);
    }

    [TestMethod]
    public void FindAllREWhenValueOccursMoreThanOnce()
    {
        //Assemble
        int[] array1 = { 5, 22, 21, 13, 21, 97, 21, 101, 102 };
        int[] array2 = { 102, 97, 21, 98, 21, 5 };
        int[] expected = { 102, 97, 21, 5 };

        //Act
        int[] result = TaskSolutions.GetRecuringElements(array1, array2);

        //Assert
        CollectionAssert.AreEquivalent(expected, result);
    }
}
