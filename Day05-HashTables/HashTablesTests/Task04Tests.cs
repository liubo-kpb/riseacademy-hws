﻿namespace HashTablesTests;
using Day05_HashTables;

[TestClass]
public class Task04Tests
{
    [TestMethod]
    public void TestSpelling()
    {
        //Assert
        HashSet<string> dictionary = new HashSet<string>(new string[] { "word", "elevate", "winner", "punch", "joy", "work" });
        string document = "winner wrod pnch elevate jou worc punch wodr";
        List<string> expected = new List<string>(new string[] { "wrod", "jou", "pnch", "worc", "wodr" });

        //Act
        List<string> result = TaskSolutions.CheckSpelling(dictionary, document);

        //Assert
        CollectionAssert.AreEquivalent(expected, result);
    }

    [TestMethod]
    public void TestSpellingWhenAllCorrect()
    {
        //Assert
        HashSet<string> dictionary = new HashSet<string>(new string[] { "word", "elevate", "winner", "punch", "joy", "work" });
        string document = "winner elevate punch";
        List<string> expected = new List<string>();

        //Act
        List<string> result = TaskSolutions.CheckSpelling(dictionary, document);

        //Assert
        CollectionAssert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestSpellingWithAdditionalWords()
    {
        //Assert
        HashSet<string> dictionary = new HashSet<string>(new string[] { "word", "elevate", "winner", "punch", "joy", "work" });
        string document = "winner wrod pnch elevate jou worc punch wodr ship door flower";
        List<string> expected = new List<string>(new string[] { "wrod", "jou", "pnch", "worc", "wodr" , "ship", "door", "flower"});

        //Act
        List<string> result = TaskSolutions.CheckSpelling(dictionary, document);

        //Assert
        CollectionAssert.AreEquivalent(expected, result);
    }

    [TestMethod]
    public void TestWhenDictionaryIsEmpty()
    {
        //Assert
        HashSet<string> dictionary = new HashSet<string>(new string[] { "word", "elevate", "winner", "punch", "joy", "work" });
        string document = "";
        string expected = "Must provide input for both variables!";

        //Act
        Assert.ThrowsException<ArgumentException>(() => TaskSolutions.CheckSpelling(dictionary, document), expected);
    }

    [TestMethod]
    public void TestWhenDocumentIsEmpty()
    {
        //Assert
        HashSet<string> dictionary = new HashSet<string>();
        string document = "winner wrod pnch elevate jou worc punch wodr ship door flower";
        string expected = "Must provide input for both variables!";

        //Act
        Assert.ThrowsException<ArgumentException>(() => TaskSolutions.CheckSpelling(dictionary, document), expected);
    }
}
