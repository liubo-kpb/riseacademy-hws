﻿namespace Day07_Graph;

public class GraphImplementation
{
    private int vertexCount;
    private List<List<int>> matrix;

    public GraphImplementation(int count)
    {
        vertexCount = count;
        matrix = new List<List<int>>();
        for (int i = 0; i < vertexCount; i++)
        {
            matrix.Add(new List<int>());
            for (int j = 0; j < vertexCount; j++)
            {
                matrix[i].Add(0);
            }
        }
    }

    public void AddEdge(int start, int end)
    {
        matrix[start][end] = 1;
        matrix[end][start] = 1;
    }

    public void AddOneWayEdge(int start, int end)
    {
        matrix[start][end] = 1;
    }

    public void DFSRecursive(int start, HashSet<int> visited)
    {
        visited.Add(start);

        for (int i = 0; i < vertexCount; i++)
        {
            if (matrix[start][i] == 1 && !visited.Contains(i))
            {
                DFSRecursive(i, visited);
            }
        }
    }

    //Exercise 1
    public bool DFSCycleFinder(int start, List<int> visited)
    {
        visited.Add(start);

        for (int i = 0; i < vertexCount; i++)
        {
            if (matrix[start][i] == 1 && visited.Contains(i))
            {
                List<int> connections = GetConnections(start);
                if (connections.Count > 1 && AreConnectionsVisited(connections, visited))
                {
                    return true;
                }
            }
            if (matrix[start][i] == 1 && !visited.Contains(i) && DFSCycleFinder(i, visited))
            {
                return true;
            }
        }
        visited.Remove(start);

        return false;
    }

    private bool AreConnectionsVisited(List<int> connections, List<int> visited)
    {
        for (int i = 0; i < connections.Count; i++)
        {
            if (!visited.Contains(connections[i]))
            {
                return false;
            }
        }

        return true;
    }

    private List<int> GetConnections(int start)
    {
        List<int> connections = new List<int>();
        for (int i = 0; i < vertexCount; i++)
        {
            if (matrix[start][i] == 1)
            {
                connections.Add(i);
            }
        }

        return connections;
    }
    //End of Exercise 1

    public int FindVertexValue(List<GraphImplementation> form, int start)
    {
        List<int> visited = new List<int>();
        if (DFSCycleFinder(start, visited))
        {
            throw new ArgumentException("Cannot establish value. Graph is cycled.");
        }

        HashSet<int> iteratedVertexes = new HashSet<int>();
        int result = DFSValueFinder(start, iteratedVertexes);
        
        return result;
    }

    public int DFSValueFinder(int start, HashSet<int> visited)
    {
        visited.Add(start);

        int result = 0;
        for (int i = 0; i < vertexCount; i++)
        {
            if (matrix[start][i] == 1 && !visited.Contains(i))
            {
                result = DFSValueFinder(i, visited);
                if (result < 0)
                {
                    throw new ArgumentException("Cannot establish value. Graph has a two way connection between two dependent sub-vertexes.");
                }
            }
            else if (matrix[start][i] == 1 && visited.Contains(i))
            {
                throw new ArgumentException("Cannot establish value. Graph has a two way connection between two dependent sub-vertexes.");
            }
        }

        return result;
    }
}
