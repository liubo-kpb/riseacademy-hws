namespace GraphCyclingTests;

using Day07_Graph;

[TestClass]
public class FindCyclesTests
{
    [TestMethod]
    public void TestCycleFinderWhenTrue()
    {
        //Assemble
        var graph = new GraphImplementation(5);
        graph.AddEdge(0, 1);
        graph.AddEdge(0, 2);
        graph.AddEdge(0, 3);
        graph.AddEdge(3, 4);
        graph.AddEdge(2, 3);
        List<int> visited = new List<int>();

        //Act
        bool result = graph.DFSCycleFinder(0, visited);

        //Assert
        Assert.IsTrue(result);
    }

    [TestMethod]
    public void TestCycleFinderWhenFalse()
    {
        //Assemble
        var graph = new GraphImplementation(5);
        graph.AddEdge(0, 1);
        graph.AddEdge(0, 2);
        graph.AddEdge(0, 3);
        graph.AddEdge(3, 4);
        List<int> visited = new List<int>();

        //Act
        bool result = graph.DFSCycleFinder(0, visited);

        //Assert
        Assert.IsFalse(result);
    }

    [TestMethod]
    public void TestCycleFinderWhenPetyaWantsToBreakIt()
    {
        //Assemble
        var graph = new GraphImplementation(6);

        graph.AddEdge(0, 3);
        graph.AddEdge(3, 2);
        graph.AddEdge(3, 4);
        graph.AddEdge(1, 2);
        graph.AddEdge(4, 5);

        List<int> visited = new List<int>();

        //Act
        bool result = graph.DFSCycleFinder(0, visited);

        //Assert
        Assert.IsFalse(result);
    }

    [TestMethod]
    public void TestCycleFinderWithBiggerCycle()
    {
        //Assemble
        var graph = new GraphImplementation(8);

        graph.AddEdge(0, 1);
        graph.AddEdge(1, 2);
        graph.AddEdge(2, 3);
        graph.AddEdge(3, 4);
        graph.AddEdge(4, 0);
        graph.AddEdge(1, 5);
        graph.AddEdge(3, 6);
        graph.AddEdge(6, 7);

        List<int> visited = new List<int>();

        //Act
        bool result = graph.DFSCycleFinder(0, visited);

        //Assert
        Assert.IsTrue(result);
    }

    [TestMethod]
    public void TestCycleFinderWithTwoElements()
    {
        //Assemble
        var graph = new GraphImplementation(8);

        graph.AddEdge(0, 1);

        List<int> visited = new List<int>();

        //Act
        bool result = graph.DFSCycleFinder(0, visited);

        //Assert
        Assert.IsFalse(result);
    }
}