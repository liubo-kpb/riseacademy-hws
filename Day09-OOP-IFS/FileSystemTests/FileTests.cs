﻿namespace FileSystemTests;
using Day09_OOP_IFS;

[TestClass]
public class FileTests
{
    [TestMethod]
    public void TestPrintFileContent()
    {
        //Assemble
        Folder folder = new Folder("home", null, "home/");
        FileGeneric file = new FileGeneric("file1.txt", "home/file1.txt", folder);
        string text = "line";
        for (int i = 1; i <= 5; i++)
        {
            file.WriteToFile(i, $"{text} {i}");
        }
        string expected = $"line 1" +
                          $"{Environment.NewLine}line 2" +
                          $"{Environment.NewLine}line 3" +
                          $"{Environment.NewLine}line 4" +
                          $"{Environment.NewLine}line 5";

        //Act
        string result = file.GetFileContent();

        //Assert
        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestPrintLast10Lines()
    {
        //Assemble
        Folder folder = new Folder("home", null, "home/");
        FileGeneric file = new FileGeneric("file1.txt", "home/file1.txt", folder);
        string text = "line";
        for (int i = 1; i <= 11; i++)
        {
            file.WriteToFile(i, $"{text} {i}");
        }
        string expected = $"line 2" +
                          $"{Environment.NewLine}line 3" +
                          $"{Environment.NewLine}line 4" +
                          $"{Environment.NewLine}line 5" +
                          $"{Environment.NewLine}line 6" +
                          $"{Environment.NewLine}line 7" +
                          $"{Environment.NewLine}line 8" +
                          $"{Environment.NewLine}line 9" +
                          $"{Environment.NewLine}line 10" +
                          $"{Environment.NewLine}line 11";

        //Act
        string result = file.GetLast10Lines();

        //Assert
        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestPrintLastRequestedLines()
    {
        //Assemble
        Folder folder = new Folder("home", null, "home/");
        FileGeneric file = new FileGeneric("file1.txt", "home/file1.txt", folder);
        string text = "line";
        for (int i = 1; i <= 11; i++)
        {
            file.WriteToFile(i, $"{text} {i}");
        }
        string expected = $"line 7" +
                          $"{Environment.NewLine}line 8" +
                          $"{Environment.NewLine}line 9" +
                          $"{Environment.NewLine}line 10" +
                          $"{Environment.NewLine}line 11";

        //Act
        string result = file.GetLasRequestedLines(5);

        //Assert
        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestWriteToFile()
    {
        //Assemble
        Folder folder = new Folder("home", null, "home/");
        FileGeneric file = new FileGeneric("file1.txt", "home/file1.txt", folder);
        string text = "line";

        //Act
        bool result = false;
        for (int i = 1; i <= 11; i++)
        {
            result = file.WriteToFile(i, $"{text} {i}");
        }

        //Assert
        Assert.IsTrue(result);
    }

    [TestMethod]
    public void TestClearFile()
    {
        //Assemble
        Folder folder = new Folder("home", null, "home/");
        FileGeneric file = new FileGeneric("file1.txt", "home/file1.txt", folder);
        string text = "line";
        for (int i = 1; i <= 11; i++)
        {
            file.WriteToFile(i, $"{text} {i}");
        }

        //Act
        file.ClearFile();
        string result = file.GetFileContent();

        //Assert
        Assert.AreEqual(string.Empty, result);
    }
}
