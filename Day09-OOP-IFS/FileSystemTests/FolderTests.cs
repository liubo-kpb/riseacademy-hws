using Day09_OOP_IFS;

namespace FileSystemTests;

[TestClass]
public class FolderTests
{
    [TestMethod]
    public void TestMethodPrintFolderContentsWithNoContents()
    {
        // Arrange
        Folder home = new Folder("home", null, "home/");
        Folder folder = new Folder("folder1", home, "home/folder1/");
        string expected = "Folder is empty.";

        // Act
        string actual = folder.PrintFolderContents();

        // Assert
        Assert.AreEqual(expected, actual);
    }

    [TestMethod]
    public void TestMethodPrintFolderContentsWithContents()
    {
        // Arrange
        HashSet<Folder> folders = new HashSet<Folder>();
        Folder home = new Folder("home", null, "home/");
        home.AddFolder("folder1", folders);
        string expected = "folder1";

        // Act
        string actual = home.PrintFolderContents();

        // Assert
        Assert.AreEqual(expected, actual);
    }

    [TestMethod]
    public void TestMethodAddFolder()
    {
        // Arrange
        HashSet<Folder> folders = new HashSet<Folder>();
        Folder home = new Folder("home", null, "home/");      
        bool expected = true;

        // Act
        bool actual = home.AddFolder("folder1", folders);

        // Assert
        Assert.AreEqual(expected, actual);
    }

    [TestMethod]
    public void TestMethodAddFolderWithEmptyName()
    {
        // Arrange
        HashSet<Folder> folders = new HashSet<Folder>();
        Folder home = new Folder("", null, "");
        bool expected = false;

        // Act
        bool actual = home.AddFolder("", folders);

        // Assert
        Assert.AreEqual(expected, actual);
    }

    [TestMethod]
    public void TestMethodAddFolderWithNameThatIsUsed()
    {
        // Arrange
        HashSet<Folder> folders = new HashSet<Folder>();
        Folder home = new Folder("home", null, "home/");
        home.AddFolder("folder1", folders);

        bool expected = false;

        // Act
        bool actual = home.AddFolder("folder1", folders);

        // Assert
        Assert.AreEqual(expected, actual);
    }

    [TestMethod]
    public void TestMethodAddFile()
    {
        // Arrange
        Folder home = new Folder("home", null, "home/");

        bool expected = true;

        // Act
        bool actual = home.AddFile("file1.txt");

        // Assert
        Assert.AreEqual(expected, actual);
    }

    [TestMethod]
    public void TestMethodAddFileWithEmptyName()
    {
        // Arrange
        Folder home = new Folder("home", null, "home/");

        bool expected = false;

        // Act
        bool actual = home.AddFile("");

        // Assert
        Assert.AreEqual(expected, actual);
    }

    [TestMethod]
    public void TestMethodAddFileWithNameThatIsUsed()
    {
        // Arrange
        Folder home = new Folder("home", null, "home/");

        bool expected = false;

        // Act
        home.AddFile("file1.txt");
        bool actual = home.AddFile("file1.txt");

        // Assert
        Assert.AreEqual(expected, actual);
    }

    [TestMethod]
    public void TestMethodPrintFilesSortedDescendingBySize()
    {
        // Arrange
        Folder home = new Folder("home", null, "home/");

        home.AddFile("file12");
        home.AddFile("file21");
        home.AddFile("file53");

        foreach (var item in home.FilesInFolder)
        {
            if (item.Name == "file12")
            {
                item.WriteToFile(10, "adsadsadsadsadasdsad");
                item.WriteToFile(3, "dasdsadsdkgdjgojpskdflkadsadsadsadsadasdsad");
                item.WriteToFile(14, "flkwkem;lewf;l,few;l");
            }
            else if (item.Name == "file21")
            {
                item.WriteToFile(10, "a");
                item.WriteToFile(3, "d");
                item.WriteToFile(14, "f");
            }
        }

        string expected = $"file12" +
            $"{Environment.NewLine}file21" +
            $"{Environment.NewLine}file53";

        // Act
        string actual = home.PrintFilesSortedDescendingBySize();

        // Assert
        Assert.AreEqual(expected, actual);
    }

    [TestMethod]
    public void TestMethodPrintFilesSortedDescendingBySizeWithNoFilesInFolder()
    {
        // Arrange
        Folder home = new Folder("home", null, "home/");

        string expected = "No files in folder.";

        // Act
        string actual = home.PrintFilesSortedDescendingBySize();

        // Assert
        Assert.AreEqual(expected, actual);
    }
}