﻿namespace Day09_OOP_IFS;

public class Command
{

    public const string cd = "cd";
    public const string mkdir = "mkdir";
    public const string create_File = "create_file";
    public const string cat = "cat";
    public const string tail = "tail";
    public const string write = "write";
    public const string ls = "ls";
    public const string exit = "exit";
    public const string wc = "wc";
}
