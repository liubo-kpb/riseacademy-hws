﻿namespace Day09_OOP_IFS;

public class ExceptionMessage
{
    public const string commandNotFound = " command not recognised.";
    public const string fileNotFound = "File not found.";
    public const string wrongFolderName = "You either entered no name for the folder or it already exists.";
    public const string wrongFileName = "You either entered no name for the file or it already exists.";
    public const string wrongExtension = "File must have an extension of at least 3 symbols";
}
