﻿using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Day09_OOP_IFS;

public class Folder
{

    private int size;
    public Folder(string name, Folder parentFolder, string path)
    {
        Name = name;
        Path = path;
        FolderContent = new HashSet<string>();
        ParentFolder = parentFolder;
        FilesInFolder = new HashSet<FileGeneric>();
    }

    public string Name { get; set; }
    public string Path { get; private set; }
    public int Size
    {
        get { return size; }

        set
        {
            foreach (var file in FilesInFolder)
            {
                size += file.Size;
            }
            foreach (var folder in SubFolders)
            {
                size += folder.Size;
            }
            if (ParentFolder != null)
            {
                ParentFolder.Size = 0;//just calling the setter of the parent folder and etc. :)
            }
        }
    }
    public HashSet<string> FolderContent { get; set; }
    public Folder? ParentFolder { get; set; }
    public HashSet<FileGeneric> FilesInFolder { get; set; }
    public HashSet<Folder> SubFolders { get; set; }

    public string PrintFolderContents()
    {
        StringBuilder stringBuilder = new StringBuilder();

        if (FolderContent.Count == 0)
        {
            return "Folder is empty.";
        }

        foreach (string element in FolderContent)
        {
            stringBuilder.AppendLine(element);
        }

        return stringBuilder.ToString().Trim();
    }

    public string PrintFilesSortedDescendingBySize()
    {
        StringBuilder stringBuilder = new StringBuilder();

        if (this.FilesInFolder.Count == 0 || SubFolders.Count == 0)
        {
            return "No files in folder.";
        }

        Dictionary<string, int> folderContent = new Dictionary<string, int>();

        foreach (var file in this.FilesInFolder)
        {
            folderContent.Add(file.Name, file.Size);
        }
        foreach (var folder in SubFolders)
        {
            folderContent.Add(folder.Name, folder.Size);
        }

        foreach (var element in folderContent.OrderByDescending(file => file.Value))
        {
            stringBuilder.AppendLine($"{element.Key}");
        }

        return stringBuilder.ToString().Trim();
    }

    public bool AddFolder(string name, HashSet<Folder> allDirectories)
    {

        if (name.Length == 0 || FolderContent.Contains(name))
        {
            return false;
        }

        Folder folder = new Folder(name, this, this.Path + name + "/");

        SubFolders.Add(folder);
        FolderContent.Add(folder.Name);
        allDirectories.Add(folder);

        return true;
    }

    public bool AddFile(string name)
    {

        if (name.Length == 0 || FolderContent.Contains(name))
        {
            return false;
        }

        FileGeneric file = new FileGeneric(name, Path, this);

        FolderContent.Add(file.Name);
        FilesInFolder.Add(file);

        return true;
    }
}
