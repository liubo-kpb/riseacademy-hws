﻿namespace Day09_OOP_IFS;

using System.Text;

public class FileGeneric
{

    private int size;
    public FileGeneric(string name, string directory, Folder containingFolder)
    {
        Name = name;
        Path = directory;
        Content = new Dictionary<int, string>();
        Content[1] = string.Empty;
        ContainingFolder = containingFolder;
        Size = 0;
    }

    public string Name { get; set; }
    public string Path { get; set; }
    public int Size
    {
        get { return size; }
        private set
        {
            size = Content.Count;
            foreach (var line in Content)
            {
                size += line.Value.Length;
            }
            ContainingFolder.Size += 0;
        }
    }

    public Folder ContainingFolder { get; set; }
    public Dictionary<int, string> Content { get; set; }

    public string GetFileContent()
    {
        StringBuilder sb = new StringBuilder();
        foreach (var line in Content)
        {
            sb.AppendLine(line.Value);
        }

        return sb.ToString().Trim();
    }

    public string GetLast10Lines()
    {
        StringBuilder sb = new StringBuilder();
        if (10 >= Content.Count)
        {
            for (int index = 1; index <= Content.Count; index++)
            {
                sb.AppendLine(Content[index]);
            }
        }
        else
        {
            int startingLine = Content.Count - 10;
            for (int index = startingLine; index < Content.Count; index++)
            {
                sb.AppendLine(Content[index + 1]);
            }
        }

        return sb.ToString().TrimEnd();
    }


    public string GetLasRequestedLines(int line)
    {
        StringBuilder sb = new StringBuilder();
        if (line >= Content.Count)
        {
            for (int index = 1; index <= Content.Count; index++)
            {
                sb.AppendLine(Content[index]);
            }
        }
        else
        {
            int startingLine = Content.Count - line;
            for (int index = startingLine; index < Content.Count; index++)
            {
                sb.AppendLine(Content[index]);
            }
        }

        return sb.ToString().Trim();
    }

    public bool WriteToFile(int line, string text)
    {
        int currentLines = Content.Count;
        int lineDifference = line - currentLines;
        if (lineDifference > 1)
        {
            for (int i = currentLines + 1; i < line; i++)
            {
                Content[i] = string.Empty;
            }
        }
        Content[line] = text;
        Size += 0; //we're just calling the setter.

        return true;
    }

    public bool ClearFile() //hoped this would be one of the commands :D
    {
        Content = new Dictionary<int, string>();
        Content[1] = string.Empty;
        Size = 1;

        return true;
    }

    public string GetWordCount()
    {
        int wordCount = 0;
        foreach (var line in Content)
        {
            string[] words = line.Value.Split(" ", StringSplitOptions.RemoveEmptyEntries);
            wordCount += words.Length;
        }

        return $"File has {wordCount} words.";
    }

}
