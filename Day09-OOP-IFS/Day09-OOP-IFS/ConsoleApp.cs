﻿namespace Day09_OOP_IFS;

public class ConsoleApp
{
    private HashSet<Folder> allDirectories;
    private string currentDirectory;
    private Folder currentFolder;

    public ConsoleApp()
    {
        allDirectories = new HashSet<Folder>();
    }

    public void Run()
    {
        Folder homeFolder = new Folder("home", null, "home/");
        currentDirectory = homeFolder.Path;
        currentFolder = homeFolder;
        allDirectories.Add(homeFolder);

        while (true)
        {
            Console.WriteLine(currentDirectory);
            string command = Console.ReadLine();

            string[] cmdArgs = command.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            switch (cmdArgs[0])
            {
                case Command.cd:

                    string destination = cmdArgs[1];
                    GoToFolder(destination);
                    break;

                case Command.mkdir:

                    RunMkDir(cmdArgs);

                    break;

                case Command.create_File:

                    RunCreateFile(cmdArgs);

                    break;

                case Command.cat:

                    RunCat(command, cmdArgs);

                    break;

                case Command.tail:

                    RunTail(command, cmdArgs);

                    break;

                case Command.write:

                    RunWrite(command, cmdArgs);

                    break;

                case Command.ls:
                    RunLs(command, cmdArgs);

                    break;

                case Command.wc:

                    RunWc(command, cmdArgs);

                    break;

                case Command.exit:

                    Environment.Exit(0);
                    break;

                default:

                    Console.WriteLine(cmdArgs[0] + ExceptionMessage.commandNotFound);
                    break;
            }
        }
    }

    private void RunWc(string command, string[] cmdArgs)
    {
        bool hasPipeLine = HasPipeline(command, cmdArgs, out List<int> pipeLineIndexes);
        string text = string.Join(" ", cmdArgs.Skip(1));

        if (hasPipeLine)
        {
            int pipeIndex = pipeLineIndexes[0];
            text = string.Join(" ", cmdArgs.Skip(1).Take(pipeIndex - 2));

            if (cmdArgs[1] == "-l")
            {
                text = string.Join(" ", cmdArgs.Skip(2).Take(pipeIndex - 3));
            }
        }
        else if (cmdArgs[1] == "-l")
        {
            text = string.Join(" ", cmdArgs.Skip(2));
        }

        if (cmdArgs[1] == "-l")
        {
            if (currentFolder.FolderContent.Contains(text))
            {
                FileGeneric file = currentFolder.FilesInFolder.First(f => f.Name == text);
                Console.WriteLine($"File has {file.Content.Count} lines");
            }
            else
            {
                int lines = text.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries).Length;
                Console.WriteLine($"Text has {lines} lines");
            }
        }
        else
        {
            if (currentFolder.FolderContent.Contains(text))
            {
                FileGeneric file = currentFolder.FilesInFolder.First(f => f.Name == text);
                Console.WriteLine($"File has {file.GetWordCount()} words");
            }
            else
            {
                int words = text.Split(' ', StringSplitOptions.RemoveEmptyEntries).Length;
                Console.WriteLine($"Text has {words} words");
            }
        }

        if (hasPipeLine)
        {
            RunPipeLine(command, cmdArgs.Skip(pipeLineIndexes[0] + 1).ToArray(), pipeLineIndexes);
        }
    }

    private void RunLs(string command, string[] cmdArgs)
    {
        bool hasPipeLine = HasPipeline(command, cmdArgs, out List<int> pipeLineIndexes);
        int commandLength = cmdArgs.Length;

        if (hasPipeLine)
        {
            commandLength = pipeLineIndexes[0];
        }

        if (commandLength == 3)
        {
            Console.WriteLine(currentFolder.PrintFilesSortedDescendingBySize());
        }
        else
        {
            Console.WriteLine(currentFolder.PrintFolderContents());
        }

        if (hasPipeLine)
        {
            RunPipeLine(command, cmdArgs.Skip(commandLength + 1).ToArray(), pipeLineIndexes);
        }
    }

    private void RunWrite(string command, string[] cmdArgs)
    {
        int line = 0;
        string fileName = string.Empty;
        string text = string.Empty;
        int index = 1;

        if (cmdArgs[1] == "-overwrite")
        {
            index = 2;
        }

        fileName = cmdArgs[index];
        line = int.Parse(cmdArgs[index + 1]);
        text = string.Join(' ', cmdArgs.Skip(index + 2));

        FileGeneric file = currentFolder.FilesInFolder.First(f => f.Name == fileName);

        if (file != null)
        {
            file.WriteToFile(line, text);
        }
        else
        {
            Console.WriteLine(ExceptionMessage.fileNotFound);
        }
    }

    private void RunTail(string command, string[] cmdArgs)
    {
        bool hasPipeLine = HasPipeline(command, cmdArgs, out List<int> pipeLineIndexes);
        string fileName = cmdArgs[1];
        int commandLength = cmdArgs.Length;

        if (hasPipeLine)
        {
            int pipeIndex = pipeLineIndexes[0];
            commandLength = pipeIndex;
            if (pipeIndex > 2)//in case file name has multiple words
            {
                fileName = string.Join(" ", cmdArgs.Skip(1).Take(pipeIndex - 2));
            }
        }
        else
        {
            fileName = cmdArgs[1];
            if (cmdArgs.Length > 2)//in case file name has multiple words
            {
                fileName = string.Join(" ", cmdArgs.Skip(1));
            }
        }


        FileGeneric file = currentFolder.FilesInFolder.First(f => f.Name == fileName);

        if (file != null && commandLength == 2)
        {
            Console.WriteLine(file.GetLast10Lines());
        }
        else if (file != null && commandLength > 2)
        {
            int startingLine = int.Parse(cmdArgs[3]);
            Console.WriteLine(file.PrintLasRequestedLines(startingLine));
        }
        else
        {
            Console.WriteLine(ExceptionMessage.fileNotFound);
        }

        if (hasPipeLine)
        {
            RunPipeLine(command, cmdArgs.Skip(pipeLineIndexes[0] + 1).ToArray(), pipeLineIndexes);
        }
    }

    private void RunCat(string command, string[] cmdArgs)
    {
        bool hasPipeLine = HasPipeline(command, cmdArgs, out List<int> pipeLineIndexes);
        string fileName = cmdArgs[1];

        if (hasPipeLine)
        {
            int pipeIndex = pipeLineIndexes[0];
            if (cmdArgs.Length > 2)//in case file name has multiple words
            {
                fileName = string.Join(" ", cmdArgs.Skip(1).Take(pipeIndex - 2));
            }
        }
        else if (cmdArgs.Length > 2) //in case file name has multiple words
        {
            fileName = string.Join(" ", cmdArgs.Skip(1));
        }

        FileGeneric file = currentFolder.FilesInFolder.First(f => f.Name == fileName);
        if (file != null)
        {
            Console.WriteLine(file.GetFileContent());
        }
        else
        {
            Console.WriteLine(ExceptionMessage.fileNotFound);
        }

        if (hasPipeLine)
        {
            RunPipeLine(command, cmdArgs.Skip(pipeLineIndexes[0] + 1).ToArray(), pipeLineIndexes);
        }
    }

    private void RunMkDir(string[] cmdArgs)
    {
        string newFolderName = cmdArgs[1];
        if (cmdArgs.Length > 2) //in case folder name has multiple words
        {
            newFolderName = string.Join(" ", cmdArgs.Skip(1));
        }

        if (!currentFolder.AddFolder(newFolderName, allDirectories))
        {
            Console.WriteLine(ExceptionMessage.wrongFolderName);
        }
    }

    private void RunCd(string command, string[] cmdArgs)
    {
        string destination = cmdArgs[1];
        GoToFolder(destination);

        if (HasPipeline(command, cmdArgs, out List<int> pipeLineIndexes))
        {
            RunPipeLine(command, cmdArgs.Skip(pipeLineIndexes[0] + 1).ToArray(), pipeLineIndexes);
        }
    }

    private void RunPipeLine(string command, string[] cmdArgs, List<int> pipeLineIndexes)
    {
        pipeLineIndexes.RemoveAt(0);

        switch (cmdArgs[0])
        {
            case Command.cd:

                RunCd(command, cmdArgs);

                break;

            case Command.mkdir:

                RunMkDir(cmdArgs);

                break;

            case Command.create_File:

                RunCreateFile(cmdArgs);

                break;

            case Command.cat:

                RunCat(command, cmdArgs);

                break;

            case Command.tail:

                RunTail(command, cmdArgs);

                break;

            case Command.write:

                RunWrite(command, cmdArgs);

                break;

            case Command.ls:
                RunLs(command, cmdArgs);

                break;

            case Command.wc:

                RunWc(command, cmdArgs);

                break;

            case Command.exit:

                Environment.Exit(0);
                break;

            default:

                Console.WriteLine(cmdArgs[0] + ExceptionMessage.commandNotFound);
                break;
        }
    }

    private void RunCreateFile(string[] cmdArgs)
    {
        string fileName = cmdArgs[1];
        if (cmdArgs.Length > 2)//in case file name has multiple words
        {
            fileName = string.Join(" ", cmdArgs.Skip(1));
        }

        if (cmdArgs[^1].Contains('.') && cmdArgs[^1].Length - cmdArgs[^1].IndexOf('.') < 4)
        {
            Console.WriteLine(ExceptionMessage.wrongExtension);
            return;
        }

        if (!currentFolder.AddFile(fileName))
        {
            Console.WriteLine(ExceptionMessage.wrongFileName);
        }
    }

    private bool HasPipeline(string command, string[] cmdArgs, out List<int> pipeLineIndexes)
    {
        pipeLineIndexes = new List<int>();

        if (!command.Contains('|'))
        {
            return false;
        }

        for (int i = 2; i < cmdArgs.Length; i++) // Starting from 2 because every command needs to be followed by the desired action, i.e. most starting probable position of | is at index 2 or after
        {
            if (cmdArgs[i] == "|")
            {
                pipeLineIndexes.Add(i);
            }
        }

        return true;
    }

    private void GoToFolder(string destination)
    {
        if (destination == ".." && currentDirectory != "home/")
        {
            currentDirectory = currentFolder.ParentFolder.Path;
            currentFolder = currentFolder.ParentFolder;
        }
        else if (allDirectories.Any(f => f.Path == destination))
        {
            var newLocation = allDirectories.First(f => f.Path == destination);
            currentDirectory = newLocation.Path;
            currentFolder = newLocation;
        }
        else if (currentFolder.FolderContent.Any(f => f == destination))
        {
            var newLocation = allDirectories.First(f => f.Name == destination);
            currentDirectory = newLocation.Path;
            currentFolder = newLocation;
        }
    }
}