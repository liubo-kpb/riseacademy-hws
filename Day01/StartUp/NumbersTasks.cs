﻿namespace StartUp;

public class NumbersTasks
{
    public static bool IsDigitEven(int n) => n % 2 == 0;
    public static bool IsDigitOdd(int n) => n % 2 == 1;
    public static bool IsDigitPrime(int n)
    {
        if (n <= 1)
        {
            return false;
        }
        for (int i = 2; i * i <= n + 1; i++)
        {
            if (n % i == 0)
            {
                return false;
            }
        }
        return true;
    }
    public static int TestLimit<T>(int number) => number + 1;
    public static int FindMinElement(int[] array) => array.Min();

    public static int FindKthElement(int k, int[] array)
    {
        array = array.OrderBy(n => n).ToArray();
        return array[k - 1];
    }

    public static int GetOddOccurance(int[] array)
    {
        int oddOccuringNum = -1;

        for (int i = 0; i < array.Length; i++)
        {
            oddOccuringNum = array[i];
            int occuranceNumber = 0;
            for (int x = 0; x < array.Length; x++)
            {
                if (oddOccuringNum == array[x])
                {
                    occuranceNumber++;
                }
            }
            if (occuranceNumber % 2 == 1)
            {
                return oddOccuringNum;
            }
        }

        return -1;
    }

    public static int GetAverage(int[] array) => array.Sum() / array.Length;

    public static long Pow(int a, int b)
    {
        if (b == 0)
        {
            return 1;
        }
        else if (b == 1)
        {
            return a;
        }
        return a * Pow(a, b - 1);
    }

    public static long DoubleFactorial(int n)
    {
        long doubleFactorial = 1;
        for (int i = (n * 2); i > 1; i--)
        {
            doubleFactorial *= i;
        }
        return doubleFactorial;
    }
    public static long KthFactorial(int k, int n)
    {
        long kthFactorial = 1;
        for (int i = (n * k); i > 1; i--)
        {
            kthFactorial *= i;
        }
        return kthFactorial;
    }

    public long MaxScalarSum(int[] a, int[] b)
    {
        if (a.Length != b.Length)
        {
            throw new ArgumentException("Vectors must have equal length!");
        }
        a = a.OrderBy(x => x).ToArray();
        b = b.OrderBy(x => x).ToArray();

        long maxScalarSum = 0;
        for (int i = 0; i < a.Length; i++)
        {
            long currentSum = a[i] * b[i];
            if (currentSum > maxScalarSum)
            {
                maxScalarSum = currentSum;
            }
        }

        return maxScalarSum;
    }

    public static int MaxSpan(int[] array)
    {
        int span = 0;
        for (int i = 0; i < array.Length; i++)
        {
            int currentSpan = 0;
            for (int x = array.Length - 1; x > i ; x--)
            {
                if (array[i] == array[x])
                {
                    currentSpan = x - i + 1;
                    break;
                }
            }
            if (currentSpan > span)
            {
                span = currentSpan;
            }
        }

        return span;
    }

    public static bool EqualSumSides(int[] numbers)
    {
        if (numbers.Length <= 2)
        {
            return false;
        }

        bool sidesAreEqual = false;

        int rightSum = numbers[0];
        int leftSum = numbers[^1];

        int x = numbers.Length - 2;
        for (int i = 1; i < numbers.Length / 2; i++)
        {
            if (leftSum > rightSum)
            {
            rightSum += numbers[i];
            }
            if (rightSum > leftSum)
            {
                leftSum += numbers[x--];
            }
            if (rightSum == leftSum)
            {
                sidesAreEqual = true;
                break;
            }
        }

        return sidesAreEqual;
    }

}
