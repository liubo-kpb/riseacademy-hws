﻿using System;
using System.Collections.Immutable;
using System.Text;
using System.Web;

namespace StartUp
{
    public class TextTasks
    {
        public static string ReverseString(string argument)
        {
            char[] charArray = argument.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public static string ReverseEveryWord(string argument)
        {
            string words = string.Join(' ', argument.Split(new char[] { ' ', '.', ',', '!', '?', ';', ':', '@' }, StringSplitOptions.RemoveEmptyEntries).Select(x => new String(x.Reverse().ToArray())));
            return words;
        }

        public static bool IsPalindrome(string argument)
        {
            string reversedArgument = ReverseString(argument);
            if (reversedArgument == argument)
            {
                return true;
            }
            return false;
        }

        public static bool IsNumberPalindrome(int number)
        {
            string argument = number.ToString();
            string reversedArgument = ReverseString(argument);
            if (reversedArgument == argument)
            {
                return true;
            }
            return false;
        }

        private static bool IsNumberPalindrome(long number)
        {
            string argument = number.ToString();
            string reversedArgument = ReverseString(argument);
            if (reversedArgument == argument)
            {
                return true;
            }
            return false;
        }
        public static long GetLargestPalindrome(long number)
        {
            long lastPalendrome = 0;

            for (long i = lastPalendrome; i < number; i++)
            {
                if (IsNumberPalindrome(i) && i > lastPalendrome)
                {
                    lastPalendrome = i;
                }
            }

            return lastPalendrome;
        }

        public static string CopyChars(string input, int k)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < k; i++)
            {
                sb.Append(input);
            }

            return sb.ToString().Trim();
        }

        public static int Mentions(string word, string text)
        {
            int count = 0;
            for (int i = 0; i < text.Length; i++)
            {
                if (text.Substring(i, word.Length) == word)
                {
                    count++;
                    i += word.Length - 1;
                }
            }
            return count;
        }

        public static string DecodeUrl(string url) => HttpUtility.UrlDecode(url);

        public static string DecodeUrlManually(string url)
        {
            StringBuilder decodedUrl = new StringBuilder();

            for (int i = 0; i < url.Length; i++)
            {
                if (i >= url.Length - 3)
                {
                    decodedUrl.Append(url[i]);
                    continue;
                }
                string subString = url.Substring(i, 3);
                switch (subString)
                {
                    case "%20":
                        decodedUrl.Append(' ');
                        i += 2;
                        break;
                    case "%3A":
                        decodedUrl.Append(':');
                        i += 2;
                        break;
                    case "%3F":
                        decodedUrl.Append('?');
                        i += 2;
                        break;
                    case "%2F":
                        decodedUrl.Append('/');
                        i += 2;
                        break;
                    case "%3D":
                        decodedUrl.Append('=');
                        i += 2;
                        break;
                    default:
                        decodedUrl.Append(url[i]);
                        break;
                }
            }

            return decodedUrl.ToString().Trim();
        }

        public static int SumOfNumbers(string input)
        {
            int sum = 0;
            for (int i = 0; i < input.Length; i++)
            {
                if (char.IsDigit(input[i]))
                {
                    sum += input[i] - 48;
                }
            }

            return sum;
        }

        public static bool Anagram(string a, string b)
        {
            if (a.Length != b.Length)
            {
                return false;
            }

            char[] charsA = a.ToCharArray();
            char[] charsB = b.ToCharArray();

            Array.Sort(charsA);
            Array.Sort(charsB);

            string newA = new String(charsA);
            string newB = new String(charsB);

            if (newA == newB)
            {
                return true;
            }

            return false;
        }

        public static bool HasAnagramOf(string text, string word)
        {
            if (word.Length > text.Length)
            {
                return false;
            }

            for (int i = 0; i < text.Length - word.Length; i++)
            {
                string textSubString = text.Substring(i, word.Length);
                if (Anagram(textSubString, word))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
