using StartUp;

namespace TestNumbers
{
    [TestClass]
    public class NumberTasksTests
    {
        //Testing IsDigitEven
        [TestMethod]
        [DataRow(4)]
        [DataRow(6)]
        [DataRow(8)]
        [DataRow(10)]
        [DataRow(12)]
        [DataRow(14)]
        public void TestIsDigitEvenWhenTrue(int digit)
        {
            //Arrange
            bool Expected = true;

            //Act
            bool result = NumbersTasks.IsDigitEven(digit);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        [TestMethod]
        public void TestIsDigitEvenWhenFalse()
        {
            //Arrange
            bool Expected = false;
            int digit = 5;

            //Act
            bool result = NumbersTasks.IsDigitEven(digit);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing IsDigitOdd
        [TestMethod]
        public void TestIsDigitOddWhenTrue()
        {
            //Arrange
            bool Expected = true;
            int digit = 5;

            //Act
            bool result = NumbersTasks.IsDigitOdd(digit);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        [TestMethod]
        public void TestIsDigitOddWhenFalse()
        {
            //Arrange
            bool Expected = false;
            int digit = 4;

            //Act
            bool result = NumbersTasks.IsDigitOdd(digit);

            //Assert
            Assert.AreEqual(Expected, result);
        }


        //Testing IsDigitPrime
        [TestMethod]
        public void TestIsDigitPrimeWhen1()
        {
            //Arrange
            bool Expected = false;

            //Act
            bool result = NumbersTasks.IsDigitPrime(1);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        [TestMethod]
        public void TestIsDigitPrimeWhenTrue()
        {
            //Arrange
            bool Expected = true;
            int digit = 2;

            //Act
            bool result = NumbersTasks.IsDigitPrime(digit);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        [TestMethod]
        public void TestIsDigitPrimeWhenFalse()
        {
            //Arrange
            bool Expected = false;
            int digit = 6;

            //Act
            bool result = NumbersTasks.IsDigitPrime(digit);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing FindMinElement()
        [TestMethod]
        public void TestFindMinElement()
        {
            //Arrange
            int[] array = { 1, 2, 3, 4, 5, 6 };
            int Expected = 1;

            //Act
            int result = NumbersTasks.FindMinElement(array);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing FindKthMinElement()
        [TestMethod]
        public void TesetFindKthMinElement()
        {
            //Arrange
            int[] array = { 3, 2, 5, 6, 1, 4 };
            int k = 3;
            int Expected = 3;

            //Act
            int result = NumbersTasks.FindKthElement(k, array);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing GetOddOccurance()
        [TestMethod]
        public void TestGetOddOccurance()
        {
            //Arrange
            int[] array = { 5, 3, 3, 1, 2, 5, 3 };
            int Expected = 3;

            //Act
            int result = NumbersTasks.GetOddOccurance(array);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        [TestMethod]
        public void TestGetOddOccuranceWhenAllOccurEvenTimes()
        {
            //Arrange
            int[] array = { 5, 3, 3, 1, 2, 5, 1, 2 };
            int Expected = -1;

            //Act
            int result = NumbersTasks.GetOddOccurance(array);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing GetAverage()
        [TestMethod]
        public void TestGetAverage()
        {
            // Arrange
            int[] array = { 1, 2, 3, 4, 5 };
            int Expected = 3;

            //Act
            int result = NumbersTasks.GetAverage(array);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing Pow()
        [TestMethod]
        public void TestPow()
        {
            //Arrange
            int a = 2;
            int b = 6;
            long Expected = 64;

            //Act
            long result = NumbersTasks.Pow(a, b);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing DoubleFactorial()
        [TestMethod]
        public void TestDoubleFactorial()
        {
            //Arrange
            int number = 3;
            long Expected = 720;

            //Act
            long result = NumbersTasks.DoubleFactorial(number);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing KthFactorial
        [TestMethod]
        public void TestKthFactorial()
        {
            //Arrange
            int number = 3;
            int k = 2;
            long Expected = 720;

            //Act
            long result = NumbersTasks.KthFactorial(k, number);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing MaxScalarSum
        [TestMethod]
        public void TestMaxScalarSum()
        {

        }

        //Testing MaxSpan()
        [TestMethod]
        public void TestMaxSpan()
        {
            //Arrange
            int[] numbers = { 2, 5, 4, 1, 3, 4 };
            int Expected = 4;

            //Act
            int result = NumbersTasks.MaxSpan(numbers);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        [TestMethod]
        public void TestMaxSpanWhen4()
        {
            //Arrange
            int[] numbers = { 2, 5, 4, 1, 3, 4 };
            int Expected = 4;

            //Act
            int result = NumbersTasks.MaxSpan(numbers);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        [TestMethod]
        [DataRow(new int[] { 8, 12, 7, 1, 7, 2, 12 })]
        public void TestMaxSpanWhen6(int[] numbers)
        {
            //Arrange
            int Expected = 6;

            //Act
            int result = NumbersTasks.MaxSpan(numbers);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        [TestMethod]
        [DataRow(new int[] { 6, 3, 6, 6, 8, 4, 3, 6 })]
        public void TestMaxSpanWhen8(int[] numbers)
        {
            //Arrange
            int Expected = 8;

            //Act
            int result = NumbersTasks.MaxSpan(numbers);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing EqualSumSides()
        [TestMethod]
        public void TestEqualSumSidesWhenTrue1()
        {
            //Arrange
            int[] numbers = new int[] { 3, 0, -1, 2, 1 };
            bool Expected = true;

            //Act
            bool result = NumbersTasks.EqualSumSides(numbers);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        [TestMethod]
        public void TestEqualSumSidesWhenTrue2()
        {
            //Arrange
            int[] numbers = { 2, 1, 2, 3, 1, 4 };
            bool Expected = true;

            //Act
            bool result = NumbersTasks.EqualSumSides(numbers);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        public void TestEqualSumSidesWhenTrue3()
        {
            //Arrange
            int[] numbers = { 8, 1, 8 };
            bool Expected = true;

            //Act
            bool result = NumbersTasks.EqualSumSides(numbers);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        [TestMethod]
        [DataRow(new int[] { 8, 8 })]
        [DataRow(new int[] { 11, 3, 2, 7 })]
        public void TestEqualSumSidesWhenFalse(int[] numbers)
        {
            //Arrange
            bool Expected = false;

            //Act
            bool result = NumbersTasks.EqualSumSides(numbers);

            //Assert
            Assert.AreEqual(Expected, result);
        }

       
    }
}