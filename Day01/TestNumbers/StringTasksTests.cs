﻿using StartUp;

namespace TestMethods
{
    [TestClass]
    public class StringTasksTests
    {
        //Testing ReverseString()
        [TestMethod]
        public void TestReverseString()
        {
            //Arrange
            string str = "iuybasdb";
            string Expected = "bdsabyui";

            //Act
            string result = TextTasks.ReverseString(str);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing ReverseEveryWord()
        [TestMethod]
        public void TestReverseWords()
        {
            //Arrange
            string str = "Hello world";
            string Expected = "olleH dlrow";

            //Act
            string result = TextTasks.ReverseEveryWord(str);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing IsPalendrome()
        [TestMethod]
        [DataRow("kayak")]
        [DataRow("deified")]
        [DataRow("rotator")]
        public void TestIsPalendromeWhenTrue(string argument)
        {
            //Act
            bool result = TextTasks.IsPalindrome(argument);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [DataRow("lkjnas")]
        [DataRow("lknas")]
        [DataRow("yuijowm")]
        public void TestIsPalendromeWhenFalse(string argument)
        {
            //Act
            bool result = TextTasks.IsPalindrome(argument);

            //Assert
            Assert.IsFalse(result);
        }

        //Testing IsNumberPalendrome()
        [TestMethod]
        [DataRow(131)]
        [DataRow(232)]
        [DataRow(454)]
        public void TestIsNumberPalendromeWhenTrue(int number)
        {
            //Act
            bool result = TextTasks.IsNumberPalindrome(number);

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        [DataRow(52)]
        [DataRow(864)]
        [DataRow(132)]
        public void TestIsNumberPalendromeWhenFalse(int number)
        {
            //Act
            bool result = TextTasks.IsNumberPalindrome(number);

            //Assert
            Assert.IsFalse(result);
        }

        //Testing GetLargestPalendrome()
        [TestMethod]
        public void TestGetLargestPalendrome()
        {
            //Arrange
            long number = 344;
            long Expected = 343;

            //Act
            long result = TextTasks.GetLargestPalindrome(number);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing CopyChars()
        [TestMethod]
        public void TestCopyChars()
        {
            //Arrange
            string argument = "nbsp;";
            int k = 3;
            string Expected = "nbsp;nbsp;nbsp;";

            //Act
            string result = TextTasks.CopyChars(argument, k);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing Menstions()
        [TestMethod]
        public void TestMentions()
        {
            //Arrange
            string word = "what";
            string text = "whattfwahtfwhatawhathwatwhat";
            int Expected = 4;

            //Act
            int result = TextTasks.Mentions(word, text);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing DecodeUrlManually()
        [TestMethod]
        public void TestDecodeIrlManually()
        {
            //Arrange
            string url = "http%3A%2F%2Fwww.google.com%2Fsearch%3Fq%3DExample";
            string Expected = "http://www.google.com/search?q=Example";

            //Act
            string result = TextTasks.DecodeUrlManually(url);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing SumOfNumbers()
        [TestMethod]
        public void TestSumOfNumbers()
        {
            //Arrange
            string argument = "oij1n123nl5";
            int Expected = 12;

            //Act
            int result = TextTasks.SumOfNumbers(argument);

            //Assert
            Assert.AreEqual(Expected, result);
        }

        //Testing Anagram()
        [TestMethod]
        [DataRow("angered", "enraged")]
        [DataRow("brush", "shrub")]
        [DataRow("dictionary", "indicatory")]
        public void TestAnagramWhenTrue(string a, string b)
        {
            //Act
            bool result = TextTasks.Anagram(a, b);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [DataRow("iubasn", "piohn")]
        [DataRow("yoabv", "oinqn")]
        [DataRow("mdlapqo", "mnqloiu")]
        public void TestAnagramWhenFalse(string a, string b)
        {
            //Act
            bool result = TextTasks.Anagram(a, b);

            //Assert
            Assert.IsFalse(result);
        }

        //Testing HasAnagramOf()
        [TestMethod]
        [DataRow("AABAAABABBA", "ABBA")]
        public void TestHasAnagramOfWhenTrue(string a, string b)
        {
            //Act
            bool result = TextTasks.HasAnagramOf(a, b);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [DataRow("AABAAAABAAABA", "ABBA")]
        [DataRow("AAB", "ABBA")]
        public void TestHasAnagramOfWhenFalse(string a, string b)
        {
            //Act
            bool result = TextTasks.HasAnagramOf(a, b);

            //Assert
            Assert.IsFalse(result);
        }
    }
}
